/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Gerente;

import java.util.Observable;

/**
 *
 * @author john
 */
public class ObservadoItems extends Observable {

    private boolean itemAñadido;

    public ObservadoItems() {
    }

    public boolean isItemAñadido() {
        return itemAñadido;
    }

    public void setComboItemsConsulta(boolean ordenAñadida) {
        this.itemAñadido = ordenAñadida;
        setChanged();
        notifyObservers();
    }

}
