/*  
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gerente;

import Controladores.AdaptadorItem;
import Modelos.Item;
import Modelos.Item.CategoriaItem;
import Modelos.Item.EstadoItem;
import Modelos.Orden;
import Validaciones.Validacion;
import Validaciones.ValidarValor;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import proyectodesarolloii.VisualizadorReporte;
import java.util.*;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.*;
import net.sf.jasperreports.engine.*;
import proyectodesarolloii.Listado;

/**
 *
 * @author daniel
 */
public class Panel_consultarItem extends javax.swing.JPanel implements Observer {

    Validacion validar;
    ObservadoItems observarRegistrar;
    DefaultTableModel modeloTablaItems;
    JFileChooser filechooser;
    boolean cargarFoto;
    String rutaFoto;
    long itemConsultado;
    int fila;

    /**
     * Creates new form PanelListadoOrden
     */
    public Panel_consultarItem(ObservadoItems observado) {
        initComponents();
        validar = new Validacion();
        fila = -1;
        itemConsultado = -1;
        observarRegistrar = observado;
        txt_precio.setInputVerifier(new ValidarValor(lbl_mensaje));
        modeloTablaItems = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }

        };

        modeloTablaItems.addColumn("");
        modeloTablaItems.addColumn("Nombre");
        modeloTablaItems.addColumn("Precio");
        modeloTablaItems.addColumn("Categoria");

        tbl_informacion.setModel(modeloTablaItems);
        tbl_informacion.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
        tbl_informacion.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);

        CategoriaItem estados[] = CategoriaItem.values();
        for (CategoriaItem estado : estados) {
            jcb_categoria.addItem(estado.toString());
        }
        for (CategoriaItem estado : estados) {
            jcb_categoriaModificada.addItem(estado.toString());
        }

        btn_consultar.setEnabled(true);
        btn_consulta_p.setEnabled(true);
        btn_cancelar.setEnabled(false);
        btn_cargar.setEnabled(false);
        btn_modificar.setEnabled(false);

        jcb_categoria.setEnabled(true);
        jcb_categoriaModificada.setEnabled(false);
        jcb_estado.setEnabled(false);

        txt_precio.setEnabled(false);
        txta_descripcion.setEnabled(false);

        tbl_informacion.setEnabled(true);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane3 = new javax.swing.JScrollPane();
        panel = new javax.swing.JPanel();
        lbl_titulo = new javax.swing.JLabel();
        pnl_consultaItem = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_informacion = new javax.swing.JTable();
        jcb_categoria = new javax.swing.JComboBox<>();
        btn_consultar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        btn_consulta_p = new javax.swing.JButton();
        pnl_modificarItems = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txta_descripcion = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jcb_estado = new javax.swing.JComboBox<>();
        jcb_categoriaModificada = new javax.swing.JComboBox<>();
        txt_precio = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        pnl_foto = new javax.swing.JPanel();
        lbl_foto_item = new javax.swing.JLabel();
        btn_cargar = new javax.swing.JButton();
        btn_modificar = new javax.swing.JButton();
        btn_cancelar = new javax.swing.JButton();
        lbl_mensaje = new javax.swing.JLabel();

        setLayout(new java.awt.CardLayout());

        panel.setBackground(java.awt.Color.white);

        lbl_titulo.setFont(new java.awt.Font("Ubuntu", 0, 25)); // NOI18N
        lbl_titulo.setForeground(new java.awt.Color(159, 12, 2));
        lbl_titulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_titulo.setText("CONSULTAR ITEM");

        pnl_consultaItem.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "Órden Actual"));
        pnl_consultaItem.setName(""); // NOI18N
        pnl_consultaItem.setOpaque(false);

        tbl_informacion.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        tbl_informacion.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Title 1"
            }
        ));
        jScrollPane1.setViewportView(tbl_informacion);

        jcb_categoria.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        jcb_categoria.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione...", "BEBIDAS", "ENTRADAS", "PLATOS FUERTES", "POSTRES", "OTROS", "TODO" }));

        btn_consultar.setBackground(new java.awt.Color(159, 12, 12));
        btn_consultar.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        btn_consultar.setForeground(java.awt.Color.white);
        btn_consultar.setText("Consultar");
        btn_consultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_consultarActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        jLabel2.setText("Categoria:");

        btn_consulta_p.setBackground(new java.awt.Color(159, 12, 12));
        btn_consulta_p.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        btn_consulta_p.setForeground(java.awt.Color.white);
        btn_consulta_p.setText("Consulta Particular");
        btn_consulta_p.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_consulta_pActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnl_consultaItemLayout = new javax.swing.GroupLayout(pnl_consultaItem);
        pnl_consultaItem.setLayout(pnl_consultaItemLayout);
        pnl_consultaItemLayout.setHorizontalGroup(
            pnl_consultaItemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_consultaItemLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_consultaItemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_consultaItemLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGap(3, 3, 3))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_consultaItemLayout.createSequentialGroup()
                        .addGap(0, 56, Short.MAX_VALUE)
                        .addGroup(pnl_consultaItemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_consultaItemLayout.createSequentialGroup()
                                .addComponent(btn_consultar, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(117, 117, 117))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_consultaItemLayout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jcb_categoria, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(67, 67, 67))))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_consultaItemLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_consulta_p, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(127, 127, 127))
        );
        pnl_consultaItemLayout.setVerticalGroup(
            pnl_consultaItemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_consultaItemLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(pnl_consultaItemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcb_categoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(8, 8, 8)
                .addComponent(btn_consultar)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 259, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(btn_consulta_p)
                .addContainerGap(258, Short.MAX_VALUE))
        );

        pnl_modificarItems.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "Órden Actual"));
        pnl_modificarItems.setName(""); // NOI18N
        pnl_modificarItems.setOpaque(false);

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        jLabel1.setText("Descripcion:");

        txta_descripcion.setColumns(20);
        txta_descripcion.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        txta_descripcion.setRows(5);
        jScrollPane2.setViewportView(txta_descripcion);

        jLabel4.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        jLabel4.setText("Categoria:");

        jLabel5.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        jLabel5.setText("Estado:");

        jcb_estado.setBackground(new java.awt.Color(254, 254, 254));
        jcb_estado.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        jcb_estado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione...", "ACTIVO", "INACTIVO" }));

        jcb_categoriaModificada.setBackground(new java.awt.Color(254, 254, 254));
        jcb_categoriaModificada.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        jcb_categoriaModificada.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione..." }));

        txt_precio.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        txt_precio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_precioKeyTyped(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        jLabel3.setText("Precio:");

        pnl_foto.setBackground(java.awt.Color.white);
        pnl_foto.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        lbl_foto_item.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/meat.png"))); // NOI18N

        javax.swing.GroupLayout pnl_fotoLayout = new javax.swing.GroupLayout(pnl_foto);
        pnl_foto.setLayout(pnl_fotoLayout);
        pnl_fotoLayout.setHorizontalGroup(
            pnl_fotoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_fotoLayout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(lbl_foto_item, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(41, Short.MAX_VALUE))
        );
        pnl_fotoLayout.setVerticalGroup(
            pnl_fotoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_fotoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_foto_item, javax.swing.GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
                .addContainerGap())
        );

        btn_cargar.setBackground(new java.awt.Color(159, 12, 12));
        btn_cargar.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        btn_cargar.setForeground(java.awt.Color.white);
        btn_cargar.setText("Cargar");
        btn_cargar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cargarActionPerformed(evt);
            }
        });

        btn_modificar.setBackground(new java.awt.Color(159, 12, 12));
        btn_modificar.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        btn_modificar.setForeground(java.awt.Color.white);
        btn_modificar.setText("Modificar");
        btn_modificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_modificarActionPerformed(evt);
            }
        });

        btn_cancelar.setBackground(new java.awt.Color(159, 12, 12));
        btn_cancelar.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        btn_cancelar.setForeground(java.awt.Color.white);
        btn_cancelar.setText("Cancelar");
        btn_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarActionPerformed(evt);
            }
        });

        lbl_mensaje.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N

        javax.swing.GroupLayout pnl_modificarItemsLayout = new javax.swing.GroupLayout(pnl_modificarItems);
        pnl_modificarItems.setLayout(pnl_modificarItemsLayout);
        pnl_modificarItemsLayout.setHorizontalGroup(
            pnl_modificarItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_modificarItemsLayout.createSequentialGroup()
                .addGroup(pnl_modificarItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_modificarItemsLayout.createSequentialGroup()
                        .addGap(64, 64, 64)
                        .addComponent(btn_modificar, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(53, 53, 53)
                        .addComponent(btn_cancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnl_modificarItemsLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pnl_modificarItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_modificarItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel1)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 398, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(pnl_modificarItemsLayout.createSequentialGroup()
                                    .addGroup(pnl_modificarItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel4)
                                        .addComponent(jLabel3)
                                        .addComponent(jLabel5))
                                    .addGap(18, 18, 18)
                                    .addGroup(pnl_modificarItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(lbl_mensaje, javax.swing.GroupLayout.DEFAULT_SIZE, 176, Short.MAX_VALUE)
                                        .addComponent(txt_precio)
                                        .addComponent(jcb_categoriaModificada, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jcb_estado, 0, 176, Short.MAX_VALUE))))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_modificarItemsLayout.createSequentialGroup()
                                .addComponent(btn_cargar, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(pnl_foto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(0, 18, Short.MAX_VALUE))
        );
        pnl_modificarItemsLayout.setVerticalGroup(
            pnl_modificarItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_modificarItemsLayout.createSequentialGroup()
                .addContainerGap(30, Short.MAX_VALUE)
                .addGroup(pnl_modificarItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txt_precio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl_mensaje, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnl_modificarItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcb_categoriaModificada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(17, 17, 17)
                .addGroup(pnl_modificarItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jcb_estado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(pnl_modificarItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnl_foto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnl_modificarItemsLayout.createSequentialGroup()
                        .addGap(116, 116, 116)
                        .addComponent(btn_cargar)))
                .addGap(18, 18, 18)
                .addGroup(pnl_modificarItemsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_modificar)
                    .addComponent(btn_cancelar))
                .addGap(26, 26, 26))
        );

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelLayout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(pnl_consultaItem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(pnl_modificarItems, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lbl_titulo, javax.swing.GroupLayout.PREFERRED_SIZE, 913, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(891, Short.MAX_VALUE))
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(pnl_consultaItem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelLayout.createSequentialGroup()
                        .addComponent(lbl_titulo, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(36, 36, 36)
                        .addComponent(pnl_modificarItems, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(661, Short.MAX_VALUE))
        );

        jScrollPane3.setViewportView(panel);

        add(jScrollPane3, "card2");
    }// </editor-fold>//GEN-END:initComponents

    private void btn_consulta_pActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_consulta_pActionPerformed

        TableModel tabla = tbl_informacion.getModel();
        fila = tbl_informacion.getSelectedRow();

        if (fila >= 0) {

            long id = (long) tabla.getValueAt(fila, 0);
            itemConsultado = id;
            AdaptadorItem adaptador = new AdaptadorItem();
            Item item_consultar = adaptador.buscarItem(id);
            String dataFoto = item_consultar.getFoto();
            double precio = item_consultar.getPrecio();

            txt_precio.setText(String.valueOf(precio));

            if (dataFoto != null) {

                BufferedImage imagen = mostrarImagen(dataFoto);
                ImageIcon foto = new ImageIcon(imagen);
                ImageIcon tmpIcon = new ImageIcon(foto.getImage().getScaledInstance(150, 190, Image.SCALE_DEFAULT));
                lbl_foto_item.setIcon(tmpIcon);

            }

            seleccionarCategoria(item_consultar);
            seleccionarEstado(item_consultar);

            txta_descripcion.setText(item_consultar.getDescripcion());

            adaptador.cerrarEntityManagerFactory();

            btn_consultar.setEnabled(false);
            btn_consulta_p.setEnabled(false);
            btn_cancelar.setEnabled(true);
            btn_cargar.setEnabled(true);
            btn_modificar.setEnabled(true);

            jcb_categoria.setEnabled(false);
            jcb_categoriaModificada.setEnabled(true);
            jcb_estado.setEnabled(true);

            txt_precio.setEnabled(true);
            txta_descripcion.setEnabled(true);

            tbl_informacion.setEnabled(true);
        } else {
            JOptionPane.showMessageDialog(null, "porfavor seleccione un item de la tabla");
        }

    }//GEN-LAST:event_btn_consulta_pActionPerformed

    private void seleccionarCategoria(Item itemConsultar) {
        int index = 1;
        CategoriaItem categoria = itemConsultar.getCategoria();
        CategoriaItem categorias[] = CategoriaItem.values();
        for (CategoriaItem categoriaComparar : categorias) {
            if (categoriaComparar == categoria) {
                jcb_categoriaModificada.setSelectedIndex(index);
            }
            index++;
        }

    }

    private void seleccionarEstado(Item itemConsultar) {
        int index = 1;
        EstadoItem estado = itemConsultar.getEstado();
        EstadoItem estados[] = EstadoItem.values();
        for (EstadoItem estadoComparar : estados) {
            if (estadoComparar == estado) {
                jcb_estado.setSelectedIndex(index);
            }
            index++;
        }
    }

    private void btn_modificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_modificarActionPerformed

        if (itemConsultado >= 0) {

            boolean resultado = false;
            AdaptadorItem adaptador = new AdaptadorItem();
            Item item_modificar = adaptador.buscarItem(itemConsultado);
            double precio = 0.0;

            if (!txt_precio.getText().isEmpty()) {
                precio = Double.parseDouble(txt_precio.getText());
            }

            String categoria = (String) jcb_categoriaModificada.getSelectedItem();
            String estado = (String) jcb_estado.getSelectedItem();
            String descripcion = txta_descripcion.getText().toUpperCase();

            if (verificarCategoria(categoria) && precio > 0 && !descripcion.isEmpty() && verificarEstado(estado)
                    && txt_precio.getInputVerifier().verify(txt_precio)) {

                EstadoItem estadoEnviar = EstadoItem.values()[jcb_estado.getSelectedIndex() - 1];
                CategoriaItem categoriaEnviar = CategoriaItem.values()[jcb_categoriaModificada.getSelectedIndex() - 1];

                if (cargarFoto) {

                    item_modificar.setDescripcion(descripcion);
                    item_modificar.setPrecio(precio);
                    item_modificar.setEstado(estadoEnviar);
                    item_modificar.setCategoria(categoriaEnviar);
                    item_modificar.setFoto(null);
                    resultado = adaptador.modificarItem(item_modificar);
                } else {
                    item_modificar.setDescripcion(descripcion);
                    item_modificar.setPrecio(precio);
                    item_modificar.setEstado(estadoEnviar);
                    item_modificar.setCategoria(categoriaEnviar);
                    resultado = adaptador.modificarItem(item_modificar);

                }
            } else {
                if (!txt_precio.getInputVerifier().verify(txt_precio)) {
                    JOptionPane.showMessageDialog(null, "El precio debe ser un numero con dos decimales y que no inicie en 0", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "Asegurese de ingresar todos los campos, la foto es opcional", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }

            adaptador.cerrarEntityManagerFactory();
            if (resultado) {
                JOptionPane.showMessageDialog(null, "Item modificado exitosamente", "Exito", JOptionPane.INFORMATION_MESSAGE);
                actualizarItemModificado(item_modificar);
                itemConsultado = -1;
                fila = -1;
                btn_consultar.setEnabled(true);
                btn_consulta_p.setEnabled(true);
                btn_cancelar.setEnabled(false);
                btn_cargar.setEnabled(false);
                btn_modificar.setEnabled(false);

                jcb_categoria.setEnabled(true);
                jcb_categoriaModificada.setEnabled(false);
                jcb_estado.setEnabled(false);

                txt_precio.setEnabled(false);
                txta_descripcion.setEnabled(false);

                jcb_categoriaModificada.setSelectedIndex(0);
                jcb_estado.setSelectedIndex(0);

                tbl_informacion.setEnabled(true);

                txta_descripcion.setText("");
                txt_precio.setText("");
                lbl_foto_item.setIcon(new ImageIcon(getClass().getResource("/img/meat.png")));

                cargarFoto = false;
                rutaFoto = null;               

            }
        } else {
            JOptionPane.showMessageDialog(null, "Error al modificar el item", "Error", JOptionPane.ERROR_MESSAGE);
        }


    }//GEN-LAST:event_btn_modificarActionPerformed

    private void actualizarItemModificado(Item item_modificar) {

        Object[] datos = new Object[4];
        datos[0] = item_modificar.getId();
        datos[1] = item_modificar.getNombre();
        datos[2] = item_modificar.getPrecio();
        datos[3] = item_modificar.getCategoria();

        for (int i = 0; i < tbl_informacion.getColumnCount(); i++) {
            tbl_informacion.getModel().setValueAt(datos[i], fila, i);
        }
    }

    private boolean verificarCategoria(String categoria) {
        boolean verificar = false;
        String[] arrayOfStrings;
        arrayOfStrings = new String[CategoriaItem.values().length];
        for (int indice = 0; indice < arrayOfStrings.length; indice++) {
            arrayOfStrings[indice] = CategoriaItem.values()[indice].toString();
        }

        for (String arrayOfString : arrayOfStrings) {
            if (categoria.equals(arrayOfString)) {
                verificar = true;
            }
        }

        return verificar;
    }

    private boolean verificarEstado(String estado) {
        boolean verificar = false;
        String[] arrayOfStrings;
        arrayOfStrings = new String[2];
        arrayOfStrings[0] = "ACTIVO";
        arrayOfStrings[1] = "INACTIVO";

        for (int i = 0; i < arrayOfStrings.length; i++) {
            if (estado.equals(arrayOfStrings[i])) {
                verificar = true;
            }
        }

        return verificar;
    }

    private BufferedImage mostrarImagen(String imagen) {
        BufferedImage img = null;
        byte[] imageByte;

        imageByte = Base64.getDecoder().decode(imagen);
        ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
        try {
            img = ImageIO.read(bis);
            bis.close();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Error al mostrar en foto" + ex);
        }

        return img;
    }


    private void btn_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarActionPerformed
        itemConsultado = -1;
        fila = -1;
        btn_consultar.setEnabled(true);
        btn_consulta_p.setEnabled(true);
        btn_cancelar.setEnabled(false);
        btn_cargar.setEnabled(false);
        btn_modificar.setEnabled(false);

        jcb_categoria.setEnabled(true);
        jcb_categoriaModificada.setEnabled(false);
        jcb_estado.setEnabled(false);

        txt_precio.setEnabled(false);
        txta_descripcion.setEnabled(false);

        jcb_categoriaModificada.setSelectedIndex(0);
        jcb_estado.setSelectedIndex(0);

        tbl_informacion.setEnabled(true);

        txta_descripcion.setText("");
        txt_precio.setText("");
        lbl_foto_item.setIcon(new ImageIcon(getClass().getResource("/img/meat.png")));

        cargarFoto = false;
        rutaFoto = null;

    }//GEN-LAST:event_btn_cancelarActionPerformed

    private void btn_cargarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cargarActionPerformed
        int returnVal = filechooser.showOpenDialog(pnl_foto);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            //Se abre el archivo seleccionado
            File file = filechooser.getSelectedFile();
            System.out.println(file.getPath());

            ImageIcon tmpIconAux = new ImageIcon(file.getPath());

            ImageIcon tmpIcon = new ImageIcon(tmpIconAux.getImage().getScaledInstance(150, 190, Image.SCALE_DEFAULT));

            lbl_foto_item.setIcon(tmpIcon);

            cargarFoto = true;
            rutaFoto = file.getPath();

        }
    }//GEN-LAST:event_btn_cargarActionPerformed

    private void btn_consultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_consultarActionPerformed

        ArrayList lista = null;
        borrarDatosTabla();
        if (jcb_categoria.getSelectedIndex() != 0) {
            String categoria = (String) jcb_categoria.getSelectedItem();
            lista = searchItemsCategory(categoria);

            Listado coleccion = new Listado(lista);
            for (Iterator iterator = coleccion.getIterator(); iterator.hasNext();) {

                Item fila = (Item) iterator.next();
                Object[] datos = new Object[4];
                datos[0] = fila.getId();
                datos[1] = fila.getNombre();
                datos[2] = fila.getPrecio();
                datos[3] = fila.getCategoria();
                modeloTablaItems.addRow(datos);

            }

        } else {
            JOptionPane.showMessageDialog(null, "Seleccione una categoria");
        }

    }//GEN-LAST:event_btn_consultarActionPerformed

    private void txt_precioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_precioKeyTyped
        validar.restringirANumeros(evt);
    }//GEN-LAST:event_txt_precioKeyTyped

    private ArrayList searchItemsCategory(String categoria) {
        ArrayList lista = null;
        AdaptadorItem adaptador = new AdaptadorItem();

        if (!categoria.equals("TODO")) {
            lista = new ArrayList(adaptador.ListadoItems(categoria));
        } else {
            lista = new ArrayList(adaptador.listadoItems());
        }

        adaptador.cerrarEntityManagerFactory();
        return lista;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancelar;
    private javax.swing.JButton btn_cargar;
    private javax.swing.JButton btn_consulta_p;
    private javax.swing.JButton btn_consultar;
    private javax.swing.JButton btn_modificar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JComboBox<String> jcb_categoria;
    private javax.swing.JComboBox<String> jcb_categoriaModificada;
    private javax.swing.JComboBox<String> jcb_estado;
    private javax.swing.JLabel lbl_foto_item;
    private javax.swing.JLabel lbl_mensaje;
    private javax.swing.JLabel lbl_titulo;
    private javax.swing.JPanel panel;
    private javax.swing.JPanel pnl_consultaItem;
    private javax.swing.JPanel pnl_foto;
    private javax.swing.JPanel pnl_modificarItems;
    private javax.swing.JTable tbl_informacion;
    private javax.swing.JTextField txt_precio;
    private javax.swing.JTextArea txta_descripcion;
    // End of variables declaration//GEN-END:variables

    public void borrarDatosTabla() {
        int tamaño = tbl_informacion.getRowCount();
        for (int i = 0; i < tamaño; i++) {
            modeloTablaItems.removeRow(0);
            System.out.println();
        }
    }

    public void reiniciarPanel() {
        borrarDatosTabla();
        itemConsultado = -1;
        fila = -1;
        btn_consultar.setEnabled(true);
        btn_consulta_p.setEnabled(true);
        btn_cancelar.setEnabled(false);
        btn_cargar.setEnabled(false);
        btn_modificar.setEnabled(false);

        jcb_categoria.setEnabled(true);
        jcb_categoriaModificada.setEnabled(false);
        jcb_estado.setEnabled(false);

        txt_precio.setEnabled(false);
        txta_descripcion.setEnabled(false);

        jcb_categoriaModificada.setSelectedIndex(0);
        jcb_estado.setSelectedIndex(0);

        tbl_informacion.setEnabled(true);

        txta_descripcion.setText("");
        txt_precio.setText("");
        lbl_foto_item.setIcon(new ImageIcon(getClass().getResource("/img/meat.png")));

        cargarFoto = false;
        rutaFoto = null;

    }

    @Override
    public void update(Observable o, Object arg) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
