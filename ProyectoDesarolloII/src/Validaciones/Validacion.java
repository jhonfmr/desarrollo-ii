/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validaciones;

import java.awt.event.KeyEvent;

/**
 *
 * @author invitado
 */
public class Validacion {

    public void Validacion() {

    }

    /**
     * Función que recibe un arreglo de campos y los recorre con el fin de
     * buscar si alguno esta vacio, de encontrarlo retorna false, de lo
     * contrario retorna true.
     *
     * @param campos
     * @return boolean que representa si los campos estan vacios o no
     */
    public boolean validarCamposVacios(String[] campos) {
        for (String campo : campos) {
            if (campo.trim().isEmpty()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Funcion que restringe a valores numericos en un campo de texto
     *
     * @param evt
     */
    public void restringirANumeros(KeyEvent evt) {
        int representacionCaracter = (int) evt.getKeyChar();
        if (!(representacionCaracter >= 48 && representacionCaracter <= 57)) {
            evt.setKeyChar((char) evt.VK_CLEAR);
        }
    }
    
    /**
     * Funcion que restringe a valores numericos y a el punto en un campo de texto
     *
     * @param evt
     */
    public void restringirANumerosDecimales(KeyEvent evt) {
        int representacionCaracter = (int) evt.getKeyChar();
        if (!(representacionCaracter >= 48 && representacionCaracter <= 57) && representacionCaracter!=46) {
            evt.setKeyChar((char) evt.VK_CLEAR);
        }
    }

    /**
     * Funcion que restringe los valores a solo alfabeticos en un campo de texto
     * incluido el espacio
     *
     * @param evt
     */
    public void restringirACaracteres(KeyEvent evt) {
        int representacionCaracter = (int) evt.getKeyChar();
        if ( representacionCaracter != 32 
                && !(representacionCaracter >= 65 && representacionCaracter <= 90)
                && !(representacionCaracter >= 97 && representacionCaracter <= 122)) {
            evt.setKeyChar((char) evt.VK_CLEAR);
        }
    }
    
    /**
     * Responsable: Sara Muñoz 
     * Funcion que verifica si una cadena es numerica
     * @param cadena
     * @return boolean
     */
    public static boolean isNumeric(String cadena){
	try {
		Integer.parseInt(cadena);
		return true;
	} catch (NumberFormatException nfe){
		return false;
	}
}
}
