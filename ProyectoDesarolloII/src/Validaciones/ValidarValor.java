/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validaciones;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author invitado
 */
public class ValidarValor extends InputVerifier {

    private Pattern pattern;
    private JLabel campo;
    private Matcher matcher;
    private static final String VALOR_PATTERN
            = "[1-9][0-9]*\\.[0-9]{1,2}";

    public ValidarValor(JLabel campo) {
        this.campo = campo;
        pattern = Pattern.compile(VALOR_PATTERN);
    }

    @Override
    public boolean verify(JComponent input) {
        JTextField tf = (JTextField) input;
        String texto = tf.getText();

        matcher = pattern.matcher(texto);
        boolean resultado = matcher.matches();
        if(resultado){
            campo.setText("Valor válido");
        }else{
            campo.setText("Valor inválido");
        }
        //System.out.println(texto + " resultado " + resultado);
        return resultado;
    }

}
