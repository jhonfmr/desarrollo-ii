/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author john
 */
@Entity
@Table(name = "facturas")
public class Factura implements Serializable {

    public Factura() {
    }

    public Factura(String fechaPago, double descuento, double propina, double iva, String cedulaCLiente, double total, String formaPago, String hora, double valorCancelado, Empleado cajero, Orden orden) {
        this.fechaPago = fechaPago;
        this.descuento = descuento;
        this.propina = propina;
        this.iva = iva;
        this.cedulaCLiente = cedulaCLiente;
        this.total = total;
        this.formaPago = formaPago;
        this.hora = hora;
        this.valorCancelado = valorCancelado;
        this.cajero = cajero;
        this.orden = orden;
    }
    
    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "generador", strategy = "increment")
    @GeneratedValue(generator = "generador")
    private Long noFactura;

    @Column(name = "fecha_pago", nullable = false)
    private String fechaPago;

    @Column(name = "valor_descuento", nullable = false)
    private double descuento;

    @Column(name = "valor_propina", nullable = false)
    private double propina;

    @Column(name = "valor_iva", nullable = false)
    private double iva;

    @Column(name = "cedula_cliente", nullable = false)
    private String cedulaCLiente;

    @Column(name = "total_pago", nullable = false)
    private double total;

    @Column(name = "forma_pago", nullable = false)
    private String formaPago;

    @Column(name = "hora_pago", nullable = false)
    private String hora;
    
    @Column(name = "valor_cancelado")
    private double valorCancelado;

     @OneToMany(mappedBy = "factura", cascade = CascadeType.ALL)
    private Collection<Pago> pagoColeccion;
    
    @OneToMany(mappedBy = "factura", cascade = CascadeType.ALL)
    private Collection<DetalleFactura> detalleFacturaColeccion;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Empleado cajero;

    @OneToOne
    @JoinColumn(nullable = false)
    private Orden orden;

    public Orden getOrden() {
        return orden;
    }

    public void setOrden(Orden orden) {
        this.orden = orden;
    }

    public Long getId() {
        return noFactura;
    }

    public void setId(Long noFactura) {
        this.noFactura = noFactura;
    }

    public Long getNoFactura() {
        return noFactura;
    }

    public void setNoFactura(Long noFactura) {
        this.noFactura = noFactura;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getCedulaCLiente() {
        return cedulaCLiente;
    }

    public void setCedulaCLiente(String cedulaCLiente) {
        this.cedulaCLiente = cedulaCLiente;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public Empleado getCajero() {
        return cajero;
    }

    public void setCajero(Empleado cajero) {
        this.cajero = cajero;
    }

    public Collection<Pago> getPagoColeccion() {
        return pagoColeccion;
    }

    public void setPagoColeccion(Collection<Pago> pagoColeccion) {
        this.pagoColeccion = pagoColeccion;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public double getPropina() {
        return propina;
    }

    public void setPropina(double propina) {
        this.propina = propina;
    }

    public double getIva() {
        return iva;
    }

    public void setIva(double iva) {
        this.iva = iva;
    }

    public Collection<DetalleFactura> getDetalleFacturaColeccion() {
        return detalleFacturaColeccion;
    }

    public void setDetalleFacturaColeccion(Collection<DetalleFactura> detalleFacturaColeccion) {
        this.detalleFacturaColeccion = detalleFacturaColeccion;
    }

    public double getValorCancelado() {
        return valorCancelado;
    }

    public void setValorCancelado(double valorCancelado) {
        this.valorCancelado = valorCancelado;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noFactura != null ? noFactura.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Factura)) {
            return false;
        }
        Factura other = (Factura) object;
        if ((this.noFactura == null && other.noFactura != null) || (this.noFactura != null && !this.noFactura.equals(other.noFactura))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelos.Pago[ noFactura=" + noFactura + " ]";
    }

}
