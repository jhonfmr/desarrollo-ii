/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author jhonfmr
 */
@Entity
@Table(name = "mesas")
public class Mesa implements Serializable {

    private static final long serialVersionUID = 1L;

    public Mesa() {
    }

    public Mesa(EstadoMesa estado, String numero_mesa, int capacidad) {
        this.estado = estado;
        this.numero_mesa = numero_mesa;
        this.capacidad = capacidad;
    }
    
    public static enum EstadoMesa {
        ACTIVO, INACTIVO;
    }

    @Id
    @GenericGenerator(name = "generador", strategy = "increment")
    @GeneratedValue(generator = "generador")
    private Long id;

    @Column(name = "estado_mesa")
    @Enumerated(value = EnumType.STRING)
    private EstadoMesa estado;

    @Column(name = "numero_mesa", unique = true)
    private String numero_mesa;

    @Column(name = "capacidad_mesa")
    private int capacidad;

    @OneToMany(mappedBy = "mesa_orden", cascade = CascadeType.ALL)
    private Collection<Orden> mesaColeccion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero_mesa() {
        return numero_mesa;
    }

    public void setNumero_mesa(String numero_mesa) {
        this.numero_mesa = numero_mesa;
    }

    public EstadoMesa getEstado() {
        return estado;
    }

    public void setEstado(EstadoMesa estado) {
        this.estado = estado;
    }
    
    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public Collection<Orden> getMesaColeccion() {
        return mesaColeccion;
    }

    public void setMesaColeccion(Collection<Orden> mesaColeccion) {
        this.mesaColeccion = mesaColeccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mesa)) {
            return false;
        }
        Mesa other = (Mesa) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelos.Mesa[ id=" + id + " ]";
    }

}
