/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author john
 */
@Entity
@Table(name = "horarios")
public class Horario implements Serializable {

    public Horario() {
    }

    public Horario(DiaHorario dia, TurnoHorario turno, Empleado empleado) {
        this.dia = dia;
        this.turno = turno;
        this.empleado = empleado;
    }

   
    
    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "generador", strategy = "increment")
    @GeneratedValue(generator = "generador")
    private Long id;

    public static enum DiaHorario {
        LUNES, MARTES,MIERCOLES,JUEVES,VIERNES,SABADO,DOMINGO;
    }      
    
    public static enum TurnoHorario {
        MATUTINO, VESPERTINO, NOCTURNO;
    }
    
    @Column(name = "dia_horario",nullable = false)
    @Enumerated(value = EnumType.STRING)
    private DiaHorario dia;
    
     @Column(name = "turno_horario",nullable = false)
    @Enumerated(value = EnumType.STRING)
    private TurnoHorario turno;
    
    
 
    
    @ManyToOne
    @JoinColumn(nullable = false)
    private Empleado empleado;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DiaHorario getDia() {
        return dia;
    }

    public void setDia(DiaHorario dia) {
        this.dia = dia;
    }   

    public TurnoHorario getTurno() {
        return turno;
    }

    public void setTurno(TurnoHorario turno) {
        this.turno = turno;
    }

 
    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Horario)) {
            return false;
        }
        Horario other = (Horario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelos.Horario[ id=" + id + " ]";
    }

}
