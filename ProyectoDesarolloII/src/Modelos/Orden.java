/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author john
 */
@Entity
@Table(name = "ordenes")
public class Orden implements Serializable {

    public Orden() {
    }

    public Orden(boolean tipo, String inicio_servicio, String fin_servicio, String fecha, EstadoOrden estado, Empleado mesero, Mesa mesa_orden) {
        this.tipo = tipo;
        this.inicio_servicio = inicio_servicio;
        this.fin_servicio = fin_servicio;
        this.fecha = fecha;
        this.estado = estado;
        this.mesero = mesero;
        this.mesa_orden = mesa_orden;
    }
    
    public static enum EstadoOrden {
        ACTIVO, CANCELADO,FINALIZADO;
    }
   
    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "generador", strategy = "increment")
    @GeneratedValue(generator = "generador")
    private Long id;
    
    @Column(name = "tipo_orden",nullable = false)
    private boolean tipo;
    
    @Column(name = "hora_inicio_servicio",nullable = false)
    private String inicio_servicio;
    
    @Column(name = "hora_fin_servicio")
    private String fin_servicio;
    
    @Column(name = "fecha_Orden",nullable = false)
    private String fecha;
    
    @Column(name = "estado_orden",nullable = false)
    @Enumerated(value = EnumType.STRING)
    private EstadoOrden estado;
    
    @ManyToOne
    @JoinColumn(nullable = false)
    private Empleado mesero;
    
    @ManyToOne
    @JoinColumn(nullable = false)
    private Mesa mesa_orden;
    
    @OneToMany(mappedBy = "orden",cascade = CascadeType.ALL)
    private Collection<Detalle> detalleColeccion;

    public Collection<Detalle> getDetalleColeccion() {
        return detalleColeccion;
    }

    public void setDetalleColeccion(Collection<Detalle> detalleColeccion) {
        this.detalleColeccion = detalleColeccion;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EstadoOrden getEstado() {
        return estado;
    }

    public void setEstado(EstadoOrden estado) {
        this.estado = estado;
    }
    
    public boolean isTipo() {
        return tipo;
    }

    public void setTipo(boolean tipo) {
        this.tipo = tipo;
    }     

    public Empleado getMesero() {
        return mesero;
    }

    public void setMesero(Empleado mesero) {
        this.mesero = mesero;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getInicio_servicio() {
        return inicio_servicio;
    }

    public void setInicio_servicio(String inicio_servicio) {
        this.inicio_servicio = inicio_servicio;
    }

    public String getFin_servicio() {
        return fin_servicio;
    }

    public void setFin_servicio(String fin_servicio) {
        this.fin_servicio = fin_servicio;
    }

    public Mesa getMesa_orden() {
        return mesa_orden;
    }

    public void setMesa_orden(Mesa mesa_orden) {
        this.mesa_orden = mesa_orden;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orden)) {
            return false;
        }
        Orden other = (Orden) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelos.Orden[ id=" + id + " ]";
    }
    
}
