/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author john
 */
@Entity
@Table(name = "empleados")
public class Empleado implements Serializable {

    public Empleado() {
    }

    public Empleado(String nombre, String apellido, String tipoDocumento, String numeroIdentificacion,
            String cargo, String foto, String direccion, String correo, String telefono, String celular, String usuario, String contraseña, EstadoEmpleado estado) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.tipoDocumento = tipoDocumento;
        this.numeroIdentificacion = numeroIdentificacion;
        this.cargo = cargo;
        this.foto = foto;
        this.direccion = direccion;
        this.correo = correo;
        this.telefono = telefono;
        this.celular = celular;
        this.usuario = usuario;
        this.contraseña = contraseña;
        this.estado = estado;
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "generador", strategy = "increment")
    @GeneratedValue(generator = "generador")
    private Long id;

    public static enum EstadoEmpleado {
        ACTIVO, INACTIVO;
    }

    @Column(name = "nombre_empleado", nullable = false)
    private String nombre;

    @Column(name = "apellido_empleado", nullable = false)
    private String apellido;

    @Column(name = "tipo_documento", nullable = false)
    private String tipoDocumento;

    @Column(name = "numero_identificacion", nullable = false, unique = true)
    private String numeroIdentificacion;

    @Column(name = "cargo_empleado", nullable = false)
    private String cargo;

    @Column(name = "foto_empleado", columnDefinition = "TEXT")
    private String foto;

    @Column(name = "direccion_empleado", nullable = false)
    private String direccion;

    @Column(name = "correo_empleado", nullable = false)
    private String correo;

    @Column(name = "telefono_empleado", nullable = false)
    private String telefono;

    @Column(name = "celular_empleado", nullable = false)
    private String celular;

    @Column(name = "usuario", nullable = false, unique = true)
    private String usuario;

    @Column(name = "contraseña", nullable = false)
    private String contraseña;

    @Column(name = "estado_empleado", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private EstadoEmpleado estado;

    @OneToMany(mappedBy = "mesero", cascade = CascadeType.ALL)
    private Collection<Orden> ordenColeccion;

    @OneToMany(mappedBy = "cajero", cascade = CascadeType.ALL)
    private Collection<Factura> facturaColeccion;

    @OneToMany(mappedBy = "empleado", cascade = CascadeType.ALL)
    private Collection<Horario> HorarioColeccion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EstadoEmpleado getEstado() {
        return estado;
    }

    public void setEstado(EstadoEmpleado estado) {
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public Collection<Orden> getOrdenColeccion() {
        return ordenColeccion;
    }

    public void setOrdenColeccion(Collection<Orden> ordenColeccion) {
        this.ordenColeccion = ordenColeccion;
    }

    public Collection<Factura> getFacturaColeccion() {
        return facturaColeccion;
    }

    public void setFacturaColeccion(Collection<Factura> facturaColeccion) {
        this.facturaColeccion = facturaColeccion;
    }

    public Collection<Horario> getHorarioColeccion() {
        return HorarioColeccion;
    }

    public void setHorarioColeccion(Collection<Horario> HorarioColeccion) {
        this.HorarioColeccion = HorarioColeccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empleado)) {
            return false;
        }
        Empleado other = (Empleado) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelos.Empleado[ id=" + id + " ]";
    }

}
