/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author john
 */
@Entity
@Table(name = "items")
public class Item implements Serializable {

    public Item() {
    }

    public Item(String foto, String descripcion, String nombre, CategoriaItem categoria, double precio, EstadoItem estado) {
        this.foto = foto;
        this.descripcion = descripcion;
        this.nombre = nombre;
        this.categoria = categoria;
        this.precio = precio;
        this.estado = estado;
    }   

    public static enum CategoriaItem {
        BEBIDAS,ENTRADAS,CERVEZAS,POSTRES,ACOMPAÑAMIENTOS,ENSALADAS,PASTAS,PARRILLA;
    }
    
    public static enum EstadoItem {
        ACTIVO,INACTIVO;
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "generador", strategy = "increment")
    @GeneratedValue(generator = "generador")
    private Long id;

    @Column(name = "foto_item", columnDefinition = "TEXT")
    private String foto;

    @Column(name = "descripcion_item", nullable = false)
    private String descripcion;

    @Column(name = "nombre_item", nullable = false,unique = true)
    private String nombre;

    @Column(name = "categoria_item", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private CategoriaItem categoria;

    @Column(name = "precio_item", nullable = false)
    private double precio;

    @Column(name = "estado_item", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private EstadoItem estado;

    @OneToMany(mappedBy = "item", cascade = CascadeType.ALL)
    private Collection<Detalle> detalleColeccion;

    public Collection<Detalle> getDetalleColeccion() {
        return detalleColeccion;
    }

    public void setDetalleColeccion(Collection<Detalle> detalleColeccion) {
        this.detalleColeccion = detalleColeccion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EstadoItem getEstado() {
        return estado;
    }

    public void setEstado(EstadoItem estado) {
        this.estado = estado;
    }
    
    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public CategoriaItem getCategoria() {
        return categoria;
    }

    public void setCategoria(CategoriaItem categoria) {
        this.categoria = categoria;
    }
    
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Item)) {
            return false;
        }
        Item other = (Item) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelos.Item[ id=" + id + " ]";
    }

}
