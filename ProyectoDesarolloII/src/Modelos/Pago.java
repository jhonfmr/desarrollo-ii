/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 *
 * @author invitado
 */
@Entity
@Table(name = "pagos")
public class Pago implements Serializable {

    public Pago() {
    }

    public Pago(double monto, FormaPago formaPago) {
        this.monto = monto;
        this.formaPago = formaPago;
    }   

    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "generador", strategy = "increment")
    @GeneratedValue(generator = "generador")
    private Long id;

    public Long getId() {
        return id;
    }

    public static enum FormaPago {
        EFECTIVO, TARJETA;
    }    

    @Column(name = "monto_pago",nullable = false)
    private double monto;  

    @Column
    @Enumerated(value = EnumType.STRING)
    private FormaPago formaPago;
    
    @ManyToOne
    @JoinColumn(nullable = false)
    private Factura factura;

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public FormaPago getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(FormaPago formaPago) {
        this.formaPago = formaPago;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pago)) {
            return false;
        }
        Pago other = (Pago) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelos.Pago[ id=" + id + " ]";
    }
    
}
