/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author john
 */
@Entity
@Table(name = "detalle")
public class Detalle implements Serializable {

    public Detalle() {
    }

   
    public Detalle(int cantidad, Orden orden, Item item) {
        this.cantidad = cantidad;
        this.orden = orden;
        this.item = item;
    }
    
    private static final long serialVersionUID = 1L;
    @Id
    @GenericGenerator(name = "generador", strategy = "increment")
    @GeneratedValue(generator = "generador")
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(nullable = false)
    private int cantidad;
    
    @ManyToOne
    @JoinColumn(nullable = false)
    private Orden orden;
    
    @ManyToOne
    @JoinColumn(nullable = false)
    private Item item;

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Orden getOrden() {
        return orden;
    }

    public void setOrden(Orden orden) {
        this.orden = orden;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Detalle)) {
            return false;
        }
        Detalle other = (Detalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelos.Detalla[ id=" + id + " ]";
    }
    
}
