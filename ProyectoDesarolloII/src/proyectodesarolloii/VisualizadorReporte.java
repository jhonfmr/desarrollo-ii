/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectodesarolloii;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author john
 */
public class VisualizadorReporte extends JasperViewer implements WindowListener {

    private static final long serialVersionUID = 1000234L;
    private static VisualizadorReporte miInstancia;// the constructor

    private VisualizadorReporte(JasperPrint jasperPrint, boolean isExitOnClose) {
        super(jasperPrint, isExitOnClose);
        addWindowListener(this);

    }

    public static VisualizadorReporte getInstance(JasperPrint print, boolean isExitOnClose) {
        if (miInstancia == null) {
            miInstancia = new VisualizadorReporte(print, isExitOnClose);
        }
        return miInstancia;
    }

    @Override
    public void windowOpened(WindowEvent e) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowClosing(WindowEvent e) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowClosed(WindowEvent e) {
        miInstancia = null;
    }

    @Override
    public void windowIconified(WindowEvent e) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowActivated(WindowEvent e) {
        //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        //To change body of generated methods, choose Tools | Templates.
    }

}
