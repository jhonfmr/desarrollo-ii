/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectodesarolloii;

import Controladores.AdaptadorFactura;
import Modelos.Factura;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author john
 */
public class FachadaReporte {

    AdaptadorFactura adaptador_factura = null;
    String user;
    String contra;
    String direccion;

    public FachadaReporte() {
        adaptador_factura = new AdaptadorFactura();
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Persistencia");
        this.user = (String) emf.getProperties().get("javax.persistence.jdbc.user");
        this.contra = "jhonfmr";
        this.direccion = (String) emf.getProperties().get("javax.persistence.jdbc.url");
        emf.close();

    }

    public void reporteMeseroMes() {
        HashMap param = new HashMap();
        try {

            GregorianCalendar fecha = new GregorianCalendar();
            int anio = fecha.get(Calendar.YEAR);
            int mes = fecha.get(Calendar.MONTH) + 1;
            String mesFinTexto = "";
            mesFinTexto = transformarNumeroMes(mes);

            param.put("mes", mes);
            param.put("anio", anio);
            param.put("mesInicioTexto", "Enero");
            param.put("mesFinTexto", mesFinTexto);
            param.put("LOGO", getClass().getResourceAsStream("/img/logo.jpg"));
            ///proyecto_desarrollo/Gerente/ReporteSedes.jasper
            // llenamos el reporte con un origen de datos vacio donde le pasamos como argumentos
            //el nombre del archvios jasper, el hasmap y una JREMptyDataSource que especifica que no habra conexion a
            // base de datos

            JasperPrint print = JasperFillManager.fillReport(getClass().getResourceAsStream("/Reportes/reporteMeseroDelMes.jasper"),
                    param, DriverManager.getConnection(direccion, user, contra));
            //lanzamos ej jasper viewer recibiendo como argumento el informe y un valor boolenano para indicar
            // que una vez cerrado el visor, no termine la aplicacion principal
            VisualizadorReporte jviewer = VisualizadorReporte.getInstance(print, false);
            //JasperViewer jviewer = new JasperViewer(print, false);
            //Establecemos el titulo del visor
            jviewer.setTitle("Reporte mesero del mes");
            jviewer.setVisible(true);

        } catch (JRException ex) {
            ex.printStackTrace();
            System.exit(1);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la conexion");
        }
    }

    public void reportePago(int nofactura) {
        HashMap param = new HashMap();
        try {

            param.put("FACTURA", nofactura);
            ///proyecto_desarrollo/Gerente/ReporteSedes.jasper
            // llenamos el reporte con un origen de datos vacio donde le pasamos como argumentos
            //el nombre del archvios jasper, el hasmap y una JREMptyDataSource que especifica que no habra conexion a
            // base de datos
            JasperPrint print = JasperFillManager.fillReport(getClass().getResourceAsStream("/Cajero/Pago.jasper"),
                    param, new JREmptyDataSource()
            //       new JRResultSetDataSource(access.pruebaGraficos())
            );

            //lanzamos ej jasper viewer recibiendo como argumento el informe y un valor boolenano para indicar
            // que una vez cerrado el visor, no termine la aplicacion principal
            VisualizadorReporte jviewer = VisualizadorReporte.getInstance(print, false);
            //JasperViewer jviewer = new JasperViewer(print, false);
            //Establecemos el titulo del visor
            jviewer.setTitle("Reporte de Pago");
            jviewer.setVisible(true);

        } catch (JRException ex) {
            ex.printStackTrace();
            System.exit(1);
        }
    }

     /**
     * Responsable: Annie Muñoz
     * @param num_factura
     * Método que genera el reporte con la información detallada de la factura con el número ingresado como parámetro
     */
    public void reporteFactura(String num_factura) {
        HashMap param = new HashMap();
        try {
            param.put("SUBREPORT_DIR", getClass().getResourceAsStream("/Cajero/items_factura.jasper"));
            param.put("LOGO", getClass().getResourceAsStream("/img/logo.jpg"));
            List<Factura> factura = adaptador_factura.buscarPorNoFactura(num_factura);
            JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(factura);
            //tring reportPath = Faces
            JasperPrint print = JasperFillManager.fillReport(getClass().getResourceAsStream("/Cajero/Factura.jasper"),
                    param, beanCollectionDataSource);

            //lanzamos ej jasper viewer recibiendo como argumento el informe y un valor boolenano para indicar
            // que una vez cerrado el visor, no termine la aplicacion principal
            VisualizadorReporte jviewer = VisualizadorReporte.getInstance(print, false);
            //JasperViewer jviewer = new JasperViewer(print, false);
            //Establecemos el titulo del visor
            jviewer.setTitle("Reporte de factura");
            jviewer.setVisible(true);

        } catch (JRException ex) {
            ex.printStackTrace();
            System.exit(1);
        }
    }

    public void reporteIMensual(int mes, int año) {
        HashMap param = new HashMap();
        try {

            //param.put("FECHA", fecha);
            //param.put("NOMBRE", nombre.toUpperCase());
            //param.put("ID", cedula);
            param.put("anio", año);
            param.put("mes", mes);
            param.put("TITULO", "Reporte de los ingresos de " + transformarNumeroMes(mes) + "-" + año);
            param.put("LOGO", getClass().getResourceAsStream("/img/logo.jpg"));

            JasperPrint print = JasperFillManager.fillReport(getClass().getResourceAsStream("/Reportes/ReporteIngresosMensuales.jasper"),
                    param, DriverManager.getConnection(direccion, user, contra));

            JasperViewer jviewer = new JasperViewer(print, false);
            //Establecemos el titulo del visor
            jviewer.setTitle("Reporte de pasajeros");
            jviewer.setVisible(true);
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Genera el reporte de los 10 items más vendidos de un mes y año en
     * especifico
     *
     * @param anio
     * @param mes
     */
    public void reporteItemsMasVendidos(int anio, int mes) {
        HashMap param = new HashMap();
        try {
            param.put("TITULO", "Top 10 de los ítems más vendidos de "
                    + transformarNumeroMes(mes) + "-" + anio);
            param.put("LOGO", getClass().getResourceAsStream("/img/logo.jpg"));
            param.put("MES", mes);
            param.put("ANIO", anio);

            JasperPrint print = JasperFillManager.fillReport(getClass().getResourceAsStream("/Reportes/ItemsMasVendidos.jasper"),
                    param, DriverManager.getConnection(direccion,
                            user, contra));

            //lanzamos ej jasper viewer recibiendo como argumento el informe y un valor boolenano para indicar
            // que una vez cerrado el visoCaptura de pantalla de 2017-06-11 13-45-24r, no termine la aplicacion principal
            VisualizadorReporte jviewer = VisualizadorReporte.getInstance(print, false);
            //JasperViewer jviewer = new JasperViewer(print, false);
            //Establecemos el titulo del visor
            jviewer.setTitle("Reporte de factura");
            jviewer.setVisible(true);

        } catch (JRException ex) {
            ex.printStackTrace();
            System.exit(1);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la conexion");
        }
    }

    /**
     * Genera el reporte de los 10 items menos de un semestre dado en un año
     * dado
     *
     * @param anio
     * @param mesInicio
     * @param mesFin
     */
    public void ReporteItemsMenosVendidos(int anio, int mesInicio, int mesFin) {
        HashMap param = new HashMap();
        try {
            param.put("TITULO", "Top 10 de los ítems menos vendidos del periodo "
                    + transformarNumeroMes(mesInicio) + "-" + transformarNumeroMes(mesFin) + " del " + anio);
            param.put("LOGO", getClass().getResourceAsStream("/img/logo.jpg"));
            param.put("MESINICIO", mesInicio);
            param.put("MESFIN", mesFin);
            param.put("ANIO", anio);

            JasperPrint print = JasperFillManager.fillReport(getClass().getResourceAsStream("/Reportes/ItemsMenosVendidos.jasper"),
                    param, DriverManager.getConnection(direccion,
                            user, contra));

            //lanzamos ej jasper viewer recibiendo como argumento el informe y un valor boolenano para indicar
            // que una vez cerrado el visoCaptura de pantalla de 2017-06-11 13-45-24r, no termine la aplicacion principal
            VisualizadorReporte jviewer = VisualizadorReporte.getInstance(print, false);
            //JasperViewer jviewer = new JasperViewer(print, false);
            //Establecemos el titulo del visor
            jviewer.setTitle("Reporte de factura");
            jviewer.setVisible(true);

        } catch (JRException ex) {
            ex.printStackTrace();
            System.exit(1);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la conexion");
        }
    }

    public void reporteTiempoPromedio() {
        HashMap param = new HashMap();
        try {

            GregorianCalendar fecha = new GregorianCalendar();
            int anio = fecha.get(Calendar.YEAR);
            int mes = fecha.get(Calendar.MONTH) + 1;
            ArrayList<Integer> meses = new ArrayList<Integer>();
            ArrayList<Integer> anios = new ArrayList<Integer>();

            meses = mesesBuscar(mes);
            anios = aniosBuscar(mes, anio);

            for (int i = 1; i <= 6; i++) {
                if (meses.get(i - 1) < meses.get(0)) {
                    int mesE = meses.get(i - 1);
                    int anioE = anios.get(0);
                    param.put("mes" + i, mesE);
                    param.put("anio" + i, anioE);
                } else {
                    int mesE = meses.get(i - 1);
                    int anioE = anios.get(1);
                    param.put("mes" + i, mesE);
                    param.put("anio" + i, anioE);
                }
            }
            param.put("LOGO", getClass().getResourceAsStream("/img/logo.jpg"));
            ///proyecto_desarrollo/Gerente/ReporteSedes.jasper
            // llenamos el reporte con un origen de datos vacio donde le pasamos como argumentos
            //el nombre del archvios jasper, el hasmap y una JREMptyDataSource que especifica que no habra conexion a
            // base de datos

            JasperPrint print = JasperFillManager.fillReport(getClass().getResourceAsStream("/Reportes/reporteTiempoAtencion.jasper"),
                    param, DriverManager.getConnection(direccion, user, contra));
            //lanzamos ej jasper viewer recibiendo como argumento el informe y un valor boolenano para indicar
            // que una vez cerrado el visor, no termine la aplicacion principal
            VisualizadorReporte jviewer = VisualizadorReporte.getInstance(print, false);
            //JasperViewer jviewer = new JasperViewer(print, false);
            //Establecemos el titulo del visor
            jviewer.setTitle("Reporte mesero del mes");
            jviewer.setVisible(true);

        } catch (JRException ex) {
            ex.printStackTrace();
            System.exit(1);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la conexion");
        }
    }

    /**
     * Responsable: Annie Muñoz
     * @param anio
     * @param mes
     * @param semana
     * Método que genera el reporte de ingresos semanales según el año, mes y semana ingresado por parámetro
     */
    public void reporteISemanal(int anio, int mes, int semana) {
        HashMap param = new HashMap();
        int inferior = 0;
        int superior = 0;

        if (semana == 1) {
            inferior = 1;
            superior = 8;
        } else if (semana == 2) {
            inferior = 9;
            superior = 16;
        } else if (semana == 3) {
            inferior = 17;
            superior = 24;
        } else if (semana == 4) {
            inferior = 25;
            superior = 31;
        }

        try {
            param.put("TITULO", "Ingresos semanales\nSemana del: " + inferior + " al " + superior + " del mes " + transformarNumeroMes(mes) + " y año " + anio);
            param.put("LOGO", getClass().getResourceAsStream("/img/logo.jpg"));
            param.put("MES", mes);
            param.put("ANIO", anio);
            param.put("LIM_INF", inferior);
            param.put("LIM_SUP", superior);
            JasperPrint print = JasperFillManager.fillReport(getClass().getResourceAsStream("/Reportes/reporteSemanal.jasper"),
                    param, DriverManager.getConnection(direccion,
                            user, contra));
            //lanzamos ej jasper viewer recibiendo como argumento el informe y un valor boolenano para indicar 
            // que una vez cerrado el visoCaptura de pantalla de 2017-06-11 13-45-24r, no termine la aplicacion principal 
            VisualizadorReporte jviewer = VisualizadorReporte.getInstance(print, false);
            //JasperViewer jviewer = new JasperViewer(print, false); 
            //Establecemos el titulo del visor 
            jviewer.setTitle("Reporte semanal");
            jviewer.setVisible(true);

        } catch (JRException ex) {
            ex.printStackTrace();
            System.exit(1);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la conexion");
        }
    }

    public ArrayList<Integer> mesesBuscar(int mesActual) {
        ArrayList<Integer> meses = new ArrayList<Integer>();

        if (mesActual < 6) {
            for (int i = 0; i < 6; i++) {
                if (mesActual - i > 0) {
                    meses.add(mesActual - i);
                } else {
                    meses.add(12 + (mesActual - i));
                }
            }
        } else {
            for (int i = 0; i < 6; i++) {
                meses.add(mesActual - i);
            }

        }

        return meses;
    }

    public ArrayList<Integer> aniosBuscar(int mesActual, int anioActual) {
        ArrayList<Integer> anios = new ArrayList<Integer>();

        if (mesActual < 6) {
            anios.add(anioActual);
            anios.add(anioActual - 1);
        } else {
            anios.add(anioActual);
            anios.add(anioActual);
        }

        return anios;
    }

    /**
     * Retorna la representacion de un mes numerico en cadena
     *
     * @param mes
     * @return mes como un string
     */
    public String transformarNumeroMes(int mes) {
        String nombreMes = "";
        switch (mes) {
            case 1:
                nombreMes = "Enero";
                break;
            case 2:
                nombreMes = "Febrero";
                break;
            case 3:
                nombreMes = "Marzo";
                break;
            case 4:
                nombreMes = "Abril";
                break;
            case 5:
                nombreMes = "Mayo";
                break;
            case 6:
                nombreMes = "Junio";
                break;
            case 7:
                nombreMes = "Julio";
                break;
            case 8:
                nombreMes = "Agosto";
                break;
            case 9:
                nombreMes = "Septiembre";
                break;
            case 10:
                nombreMes = "Octubre";
                break;
            case 11:
                nombreMes = "Noviembre";
                break;
            case 12:
                nombreMes = "Diciembre";
                break;
        }
        return nombreMes;
    }

}
