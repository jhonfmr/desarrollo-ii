/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectodesarolloii;

import Modelos.Orden;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author john
 */
public class Listado implements Contenedor{

    private ArrayList coleccion;

    public Listado(ArrayList coleccion) {
        this.coleccion = coleccion;
    }
    
    

    
    @Override
    public Iterator getIterator() {
        return new IteradorListado();
    }
    
    private class IteradorListado implements Iterator{

        private int indice = 0;
        
        @Override
        public boolean hasNext() {
            return indice < coleccion.size();
        }

        @Override
        public Object next() {
           if(this.hasNext()){
            return coleccion.get(indice++);
         }
         return null;
        }
       
    }
    
}
