/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mesero;

import java.util.Observable;

/**
 *
 * @author john
 */
public class Observado extends Observable {

    private boolean ordenAñadida;
    private long idOrden;

    public Observado() {
        this.ordenAñadida = false;
        this.idOrden = -1;
    }

    public boolean isOrdenAñadida() {
        return ordenAñadida;
    }

    public long getIdOrden() {
        return idOrden;
    }
    
    public void reinicirObservador(){
        this.idOrden = -1;
        this.ordenAñadida = false;
    }

    public void setOrdenAñadida(boolean ordenAñadida,long idOrden) {
        this.ordenAñadida = ordenAñadida;
        this.idOrden = idOrden;
        setChanged();
        notifyObservers();
    }

}
