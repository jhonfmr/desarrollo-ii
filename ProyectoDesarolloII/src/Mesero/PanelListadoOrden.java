/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mesero;

import Controladores.AdaptadorDetalle;
import Controladores.AdaptadorItem;
import Controladores.AdaptadorMesa;
import Controladores.AdaptadorOrden;
import Modelos.Detalle;
import Modelos.Item;
import Modelos.Item.CategoriaItem;
import Modelos.Mesa;
import Modelos.Orden;
import Modelos.Orden.EstadoOrden;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import proyectodesarolloii.Listado;

/**
 *
 * @author john
 */
public class PanelListadoOrden extends javax.swing.JPanel implements Observer {

    Observado miObservador;
    DefaultTableModel modeloTablaOrdenes;
    DefaultTableModel modeloTablaItems;
    DefaultTableModel modeloTablaDetalle;
    AdaptadorOrden adaptadorOrden;

    /**
     * Creates new form PanelListadoOrden
     *
     * @param observador
     * @param emf
     */
    public PanelListadoOrden(Observado observador) {
        initComponents();
        this.adaptadorOrden = new AdaptadorOrden();
        miObservador = observador;

        modeloTablaItems = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }

        };
        modeloTablaItems.addColumn("");
        modeloTablaItems.addColumn("Nombre");
        modeloTablaItems.addColumn("Descripcion");
        modeloTablaItems.addColumn("Precio");

        tbl_items.setModel(modeloTablaItems);
        tbl_items.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
        tbl_items.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);

        modeloTablaDetalle = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
            
        };
        modeloTablaDetalle.addColumn("idDetalle");
        modeloTablaDetalle.addColumn("idItem");
        modeloTablaDetalle.addColumn("Nombre");
        modeloTablaDetalle.addColumn("Cantidad");

        tbl_tablaDetalleOrden.setModel(modeloTablaDetalle);
        tbl_tablaDetalleOrden.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
        tbl_tablaDetalleOrden.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
        tbl_tablaDetalleOrden.getTableHeader().getColumnModel().getColumn(1).setMinWidth(0);
        tbl_tablaDetalleOrden.getTableHeader().getColumnModel().getColumn(1).setMaxWidth(0);

        modeloTablaOrdenes = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }

        };
        modeloTablaOrdenes.addColumn("");
        modeloTablaOrdenes.addColumn("Estado");
        modeloTablaOrdenes.addColumn("No. Mesa");
        modeloTablaOrdenes.addColumn("Hora inicio");

        tbl_tablaOrdenes.setModel(modeloTablaOrdenes);
        tbl_tablaOrdenes.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
        tbl_tablaOrdenes.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);

        jcbox_Filtro.addItem("Sin filtro");

        EstadoOrden estadosOrden[] = EstadoOrden.values();
        for (EstadoOrden estado : estadosOrden) {
            jcbox_Filtro.addItem(estado.toString());
        }

        jcbox_categoriaItems.addItem("CATEGORIA");
        CategoriaItem estadosItem[] = CategoriaItem.values();
        for (CategoriaItem estado : estadosItem) {
            jcbox_categoriaItems.addItem(estado.toString());
        }
        jcbox_noMesa.addItem("No. Mesa");

        btn_añadir.setEnabled(false);
        btn_eliminar.setEnabled(false);
        btn_modificar.setEnabled(false);
        jcbox_categoriaItems.setEnabled(false);

        cargarMesas();

    }

    /**
     * Permite cargar las ordenes a la tabla dada una lista
     *
     * @param lista
     */
    public void cargarTablaOrdenes(ArrayList lista) {
        Listado coleccion = new Listado(lista);
        for (Iterator iterator = coleccion.getIterator(); iterator.hasNext();) {
            Orden fila = (Orden) iterator.next();
            Object[] datos = new Object[5];
            datos[0] = fila.getId();
            datos[1] = fila.getEstado();
            datos[2] = fila.getMesa_orden().getNumero_mesa();
            datos[3] = fila.getInicio_servicio();
            modeloTablaOrdenes.addRow(datos);
        }
    }

    public void cerrarConexion() {
        adaptadorOrden.cerrarEntityManagerFactory();
    }

    /**
     * Funcion que determina si un item ya ha sido listado en la tabla de
     *
     * @param idItem
     * @return boolean que indica si el item ya esta listado en el detalle
     */
    public boolean buscarItemRepetido(long idItem) {
        boolean itemListado = false;
        for (int indice = 0; indice < tbl_tablaDetalleOrden.getRowCount(); indice++) {
            long valorTabla = (long) modeloTablaDetalle.getValueAt(indice, 1);
            if (idItem == valorTabla) {
                itemListado = true;
                break;
            }
        }
        return itemListado;
    }

    /**
     * Permite borrar los datos de una tabla a partir del modelo y la tabla que
     * se le ingresen por parametro
     *
     * @param modelo
     * @param tabla
     */
    public void borrarDatosTabla(DefaultTableModel modelo, JTable tabla) {
        int tamaño = tabla.getRowCount();
        for (int i = 0; i < tamaño; i++) {
            modelo.removeRow(0);
            System.out.println();
        }
    }

    /**
     * Permite cargar todas las mesas almacenadas en la base de datos
     */
    public void cargarMesas() {
        AdaptadorMesa adaptador = new AdaptadorMesa();
        ArrayList lista = new ArrayList(adaptador.listadoMesas());
        Listado listado = new Listado(lista);
        for (Iterator iterator = listado.getIterator(); iterator.hasNext();) {
            Mesa mesa = (Mesa) iterator.next();
            jcbox_noMesa.addItem(mesa.getNumero_mesa());
        }
        adaptador.cerrarEntityManagerFactory();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        pnl_principal = new javax.swing.JPanel();
        lbl_titulo = new javax.swing.JLabel();
        pnl_listadoOrden = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        areaTextoInfoOrden = new javax.swing.JTextArea();
        pnl_infoOrden = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_tablaOrdenes = new javax.swing.JTable();
        lbl_busqueda = new javax.swing.JLabel();
        jcbox_noMesa = new javax.swing.JComboBox<>();
        lbl_Filtro = new javax.swing.JLabel();
        jcbox_Filtro = new javax.swing.JComboBox<>();
        btn_marcar = new javax.swing.JButton();
        btn_visualizar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbl_tablaDetalleOrden = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        tbl_items = new javax.swing.JTable();
        btn_modificar = new javax.swing.JButton();
        btn_eliminar = new javax.swing.JButton();
        jcbox_categoriaItems = new javax.swing.JComboBox<>();
        btn_añadir = new javax.swing.JButton();
        jcb_habilitar = new javax.swing.JCheckBox();
        jcb_llevar = new javax.swing.JCheckBox();

        setLayout(new java.awt.CardLayout());

        pnl_principal.setBackground(java.awt.Color.white);

        lbl_titulo.setFont(new java.awt.Font("Ubuntu", 0, 25)); // NOI18N
        lbl_titulo.setForeground(new java.awt.Color(159, 12, 2));
        lbl_titulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_titulo.setText("CONSULTA Y/O MODIFICAR ORDEN");

        pnl_listadoOrden.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "Información de la orden"));
        pnl_listadoOrden.setOpaque(false);

        areaTextoInfoOrden.setEditable(false);
        areaTextoInfoOrden.setColumns(20);
        areaTextoInfoOrden.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        areaTextoInfoOrden.setRows(5);
        areaTextoInfoOrden.setMargin(new java.awt.Insets(10, 10, 10, 10));
        jScrollPane3.setViewportView(areaTextoInfoOrden);

        javax.swing.GroupLayout pnl_listadoOrdenLayout = new javax.swing.GroupLayout(pnl_listadoOrden);
        pnl_listadoOrden.setLayout(pnl_listadoOrdenLayout);
        pnl_listadoOrdenLayout.setHorizontalGroup(
            pnl_listadoOrdenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_listadoOrdenLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 453, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnl_listadoOrdenLayout.setVerticalGroup(
            pnl_listadoOrdenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_listadoOrdenLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 365, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnl_infoOrden.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "Listado de ordenes"));
        pnl_infoOrden.setOpaque(false);

        tbl_tablaOrdenes.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        tbl_tablaOrdenes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbl_tablaOrdenes.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(tbl_tablaOrdenes);

        lbl_busqueda.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        lbl_busqueda.setText("Búsqueda: ");

        jcbox_noMesa.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        jcbox_noMesa.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jcbox_noMesaItemStateChanged(evt);
            }
        });

        lbl_Filtro.setFont(new java.awt.Font("Ubuntu", 1, 16)); // NOI18N
        lbl_Filtro.setText("Filtro");

        jcbox_Filtro.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        jcbox_Filtro.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jcbox_FiltroItemStateChanged(evt);
            }
        });

        btn_marcar.setBackground(new java.awt.Color(159, 12, 12));
        btn_marcar.setForeground(java.awt.Color.white);
        btn_marcar.setText("Marcar orden");
        btn_marcar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_marcarActionPerformed(evt);
            }
        });

        btn_visualizar.setBackground(new java.awt.Color(159, 12, 12));
        btn_visualizar.setForeground(java.awt.Color.white);
        btn_visualizar.setText("Visualizar orden");
        btn_visualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_visualizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnl_infoOrdenLayout = new javax.swing.GroupLayout(pnl_infoOrden);
        pnl_infoOrden.setLayout(pnl_infoOrdenLayout);
        pnl_infoOrdenLayout.setHorizontalGroup(
            pnl_infoOrdenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_infoOrdenLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_infoOrdenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnl_infoOrdenLayout.createSequentialGroup()
                        .addGroup(pnl_infoOrdenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pnl_infoOrdenLayout.createSequentialGroup()
                                .addComponent(btn_marcar, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btn_visualizar)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(pnl_infoOrdenLayout.createSequentialGroup()
                        .addComponent(lbl_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jcbox_noMesa, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_Filtro, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jcbox_Filtro, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnl_infoOrdenLayout.setVerticalGroup(
            pnl_infoOrdenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_infoOrdenLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(pnl_infoOrdenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_busqueda)
                    .addComponent(jcbox_noMesa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_Filtro)
                    .addComponent(jcbox_Filtro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41)
                .addGroup(pnl_infoOrdenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_marcar, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_visualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "Modificacion de la orden"));
        jPanel1.setOpaque(false);

        tbl_tablaDetalleOrden.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbl_tablaDetalleOrden.getTableHeader().setReorderingAllowed(false);
        jScrollPane4.setViewportView(tbl_tablaDetalleOrden);

        tbl_items.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbl_items.getTableHeader().setReorderingAllowed(false);
        jScrollPane5.setViewportView(tbl_items);

        btn_modificar.setBackground(new java.awt.Color(159, 12, 12));
        btn_modificar.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        btn_modificar.setForeground(java.awt.Color.white);
        btn_modificar.setText("Modificar");
        btn_modificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_modificarActionPerformed(evt);
            }
        });

        btn_eliminar.setBackground(new java.awt.Color(159, 12, 12));
        btn_eliminar.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        btn_eliminar.setForeground(java.awt.Color.white);
        btn_eliminar.setText("Eliminar");
        btn_eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_eliminarActionPerformed(evt);
            }
        });

        jcbox_categoriaItems.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        jcbox_categoriaItems.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jcbox_categoriaItemsItemStateChanged(evt);
            }
        });

        btn_añadir.setBackground(new java.awt.Color(159, 12, 12));
        btn_añadir.setFont(new java.awt.Font("Ubuntu", 0, 16)); // NOI18N
        btn_añadir.setForeground(java.awt.Color.white);
        btn_añadir.setText("Añadir");
        btn_añadir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_añadirActionPerformed(evt);
            }
        });

        jcb_habilitar.setText("Habilitar edición");
        jcb_habilitar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcb_habilitarActionPerformed(evt);
            }
        });

        jcb_llevar.setText("Para llevar");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 461, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jcb_llevar)
                        .addGap(39, 39, 39)
                        .addComponent(btn_modificar, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_eliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jcb_habilitar))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jcbox_categoriaItems, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_añadir, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jcb_habilitar)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(0, 1, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(btn_eliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
                                    .addComponent(btn_modificar)))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jcbox_categoriaItems, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btn_añadir)))
                        .addGap(10, 10, 10))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jcb_llevar)
                        .addGap(18, 18, 18)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 358, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 358, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout pnl_principalLayout = new javax.swing.GroupLayout(pnl_principal);
        pnl_principal.setLayout(pnl_principalLayout);
        pnl_principalLayout.setHorizontalGroup(
            pnl_principalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_principalLayout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(pnl_principalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnl_principalLayout.createSequentialGroup()
                        .addComponent(pnl_infoOrden, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(pnl_listadoOrden, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbl_titulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(34, Short.MAX_VALUE))
        );
        pnl_principalLayout.setVerticalGroup(
            pnl_principalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_principalLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(lbl_titulo, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnl_principalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnl_infoOrden, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pnl_listadoOrden, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(86, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(pnl_principal);

        add(jScrollPane1, "card2");
    }// </editor-fold>//GEN-END:initComponents

    private void jcbox_FiltroItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jcbox_FiltroItemStateChanged
        borrarDatosTabla(modeloTablaOrdenes, tbl_tablaOrdenes);
        if (jcbox_Filtro.getSelectedIndex() > 0) {
            EstadoOrden estado = EstadoOrden.values()[jcbox_Filtro.getSelectedIndex() - 1];
            //Se crea una lista con los objetos recuperados de la base de datos
            ArrayList lista;
            if (jcbox_noMesa.getSelectedIndex() > 0) {
                AdaptadorMesa adaptadorMesa = new AdaptadorMesa();
                String NoMesa = jcbox_noMesa.getSelectedItem().toString();
                Mesa mesa = (Mesa) adaptadorMesa.buscarMesa(NoMesa).get(0);
                lista = new ArrayList(adaptadorOrden.listadoOrdenNoMesaEstado(mesa, estado));
                adaptadorMesa.cerrarEntityManagerFactory();
            } else {
                lista = new ArrayList(adaptadorOrden.listadoOrdenes(estado));
            }
            cargarTablaOrdenes(lista);
        } else if (jcbox_noMesa.getSelectedIndex() > 0) {
            AdaptadorMesa adaptadorMesa = new AdaptadorMesa();
            String NoMesa = jcbox_noMesa.getSelectedItem().toString();
            Mesa mesa = (Mesa) adaptadorMesa.buscarMesa(NoMesa).get(0);
            ArrayList lista = new ArrayList(adaptadorOrden.listadoOrdenes(mesa));
            cargarTablaOrdenes(lista);
            adaptadorMesa.cerrarEntityManagerFactory();
        }
    }//GEN-LAST:event_jcbox_FiltroItemStateChanged

    private void btn_visualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_visualizarActionPerformed
        int filaSeleccionada = tbl_tablaOrdenes.getSelectedRow();
        String formatoInformacion = "";
        //Solo permitira que se editen aquella orden que este seleccionada
        if (filaSeleccionada >= 0) {
            long idOrden = (long) modeloTablaOrdenes.getValueAt(filaSeleccionada, 0);
            Orden orden = adaptadorOrden.buscarOrden(idOrden);

            formatoInformacion += "Fecha de la orden: " + orden.getFecha()
                    + "\nEstado de la orden: " + orden.getEstado()
                    + "\nOrden para llevar: ";
            if (orden.isTipo()) {
                formatoInformacion += "Si";
            } else {
                formatoInformacion += "No";
            }
            formatoInformacion += "\nMesa de la orden: " + orden.getMesa_orden().getNumero_mesa()
                    + "\nHora de inicio de la orden: " + orden.getInicio_servicio()
                    + "\nHora de finalización de la orden: " + orden.getFin_servicio()
                    + "\nOrden finalizada: ";
            String estado = orden.getEstado().toString();
            if (estado.equalsIgnoreCase("FINALIZADO") || estado.equalsIgnoreCase("CANCELADO")) {
                formatoInformacion += "Si";
            } else {
                formatoInformacion += "No";
            }
            formatoInformacion += "\n\nListado de items de la orden \n";
            List<Detalle> listaDetalle = (List<Detalle>) orden.getDetalleColeccion();

            for (Detalle detalle : listaDetalle) {
                formatoInformacion += "\t" + detalle.getItem().getNombre()
                        + " x" + detalle.getCantidad() + "\n";
            }

            areaTextoInfoOrden.setText(formatoInformacion);

        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar una orden primero",
                    "Advertencia", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btn_visualizarActionPerformed

    private void btn_marcarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_marcarActionPerformed
        int filaSeleccionada = tbl_tablaOrdenes.getSelectedRow();
        //Solo permitira que se editen aquella orden que este seleccionada
        if (filaSeleccionada >= 0) {
            long idOrden = (long) modeloTablaOrdenes.getValueAt(filaSeleccionada, 0);
            Orden orden = adaptadorOrden.buscarOrden(idOrden);
            //Se toma la hora del sistema
            Calendar calendario = new GregorianCalendar();
            int hora = calendario.get(Calendar.HOUR_OF_DAY);
            int minuto = calendario.get(Calendar.MINUTE);
            //Se realiza la edicion de la orden
            orden.setFin_servicio(hora + ":" + minuto);
            orden.setEstado(EstadoOrden.FINALIZADO);

            if (adaptadorOrden.editarOrden(orden)) {
                JOptionPane.showMessageDialog(null, "La orden ha sido marcada como despachada con exito",
                        "Operación exitosa", JOptionPane.INFORMATION_MESSAGE);
                modeloTablaOrdenes.setValueAt(orden.getEstado(), filaSeleccionada, 1);
                tbl_tablaOrdenes.clearSelection();
                areaTextoInfoOrden.setText("");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar una orden primero",
                    "Advertencia", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btn_marcarActionPerformed

    private void btn_añadirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_añadirActionPerformed
        int filaSeleccionada = tbl_items.getSelectedRow();
        //Solo si se tiene una fila seleccionada de la tabla
        if (filaSeleccionada >= 0) {
            long idItem = (long) modeloTablaItems.getValueAt(filaSeleccionada, 0);
            if (!buscarItemRepetido(idItem)) {
                String nombreItem = (String) modeloTablaItems.getValueAt(filaSeleccionada, 1);
                //Mensaje que permite seleccionar la cantidad de productos
                Object seleccion = JOptionPane.showInputDialog(null, "Cantidad", "Mensaje", JOptionPane.PLAIN_MESSAGE,
                        null, new Object[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 0);
                //solo en caso de que no se haya dado cancelar al mensaje de cantidad de items
                if (seleccion != null) {
                    int cantidad = (int) seleccion;

                    Object[] fila = new Object[]{-1L, idItem, nombreItem, cantidad};
                    modeloTablaDetalle.addRow(fila);
                    //limpia cualquier posible seleccion de fila que este en la tabla
                    tbl_items.clearSelection();
                }
            } else {
                JOptionPane.showMessageDialog(null, "El item ya ha sido añadido",
                        "Selección ítem", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No ha seleccionado ningun ítem",
                    "Selección ítem", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btn_añadirActionPerformed

    private void btn_eliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_eliminarActionPerformed
        int filaSeleccionada = tbl_tablaDetalleOrden.getSelectedRow();

        if (filaSeleccionada >= 0) {
            AdaptadorDetalle adaptadorDetalle = new AdaptadorDetalle();
            long idDetalle = (long) modeloTablaDetalle.getValueAt(filaSeleccionada, 0);
            Detalle detalleOrden = adaptadorDetalle.buscarDetalleOrden(idDetalle);
            if (detalleOrden != null) {
                adaptadorDetalle.EliminarDetalle(detalleOrden);
            }
            modeloTablaDetalle.removeRow(filaSeleccionada);
            JOptionPane.showMessageDialog(null, "ítem eliminado de la orden",
                    "Eliminación del ítem", JOptionPane.INFORMATION_MESSAGE);
            adaptadorDetalle.cerrarEntityManagerFactory();
        } else {
            JOptionPane.showMessageDialog(null, "No ha seleccionado ningun ítem",
                    "Selección ítem", JOptionPane.WARNING_MESSAGE);
        }


    }//GEN-LAST:event_btn_eliminarActionPerformed

    private void btn_modificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_modificarActionPerformed
        int filaSeleccionada = tbl_tablaOrdenes.getSelectedRow();
        long idOrden = (long) modeloTablaOrdenes.getValueAt(filaSeleccionada, 0);
        Orden orden = adaptadorOrden.buscarOrden(idOrden);
        AdaptadorDetalle adaptadorDetalle = new AdaptadorDetalle();
        ArrayList<Detalle> lista = new ArrayList<Detalle>();
        AdaptadorItem adaptadorItem = new AdaptadorItem();
        //Se recorre la tabla de items que van a la orden
        for (int indice = 0; indice < tbl_tablaDetalleOrden.getRowCount(); indice++) {
            long idItem = (long) modeloTablaDetalle.getValueAt(indice, 1);
            long idDetalle = (long) modeloTablaDetalle.getValueAt(indice, 0);
            int cantidadItems = Integer.parseInt(modeloTablaDetalle.getValueAt(indice, 3).toString());
            //Cabe recalcar que la orden no es necesaria buscarla dado que como se almaceno primero,
            //el objeto que se tiene anteriormente instanciado tiene el id que le corresponde
            Detalle detalleOrden = adaptadorDetalle.buscarDetalleOrden(idDetalle);
            if (detalleOrden == null) {
                detalleOrden = new Detalle(cantidadItems, orden, adaptadorItem.buscarItem(idItem));
                adaptadorDetalle.almacenarDetalleOrden(detalleOrden);
            }
            lista.add(detalleOrden);
        }
        orden.setTipo(jcb_llevar.isSelected());
        orden.setDetalleColeccion(lista);
        if (adaptadorOrden.editarOrden(orden)) {
            JOptionPane.showMessageDialog(null, "Orden modificada exitosamente.",
                    "Modificación orden", JOptionPane.INFORMATION_MESSAGE);
        }
        jcb_habilitar.doClick();
        adaptadorItem.cerrarEntityManagerFactory();
        adaptadorDetalle.cerrarEntityManagerFactory();

    }//GEN-LAST:event_btn_modificarActionPerformed

    private void jcbox_categoriaItemsItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jcbox_categoriaItemsItemStateChanged
        borrarDatosTabla(modeloTablaItems, tbl_items);
        AdaptadorItem adaptador = new AdaptadorItem();
        if (jcbox_categoriaItems.getSelectedIndex() != 0) {
            String categoria = jcbox_categoriaItems.getSelectedItem().toString();
            //Se crea una lista con los objetos recuperados de la base de datos
            ArrayList lista = new ArrayList(adaptador.ListadoItems(categoria));
            //Se hace un listado para poder usar el patron iterador propio
            Listado listado = new Listado(lista);
            for (Iterator iterator = listado.getIterator(); iterator.hasNext();) {
                Item fila = (Item) iterator.next();
                Object[] datos = new Object[4];
                datos[0] = fila.getId();
                datos[1] = fila.getNombre();
                datos[2] = fila.getDescripcion();
                datos[3] = fila.getPrecio();
                modeloTablaItems.addRow(datos);

            }
        }
        adaptador.cerrarEntityManagerFactory();

    }//GEN-LAST:event_jcbox_categoriaItemsItemStateChanged

    private void jcb_habilitarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcb_habilitarActionPerformed
        int filaSeleccionada = tbl_tablaOrdenes.getSelectedRow();
        if (filaSeleccionada >= 0) {
            String estadoOrden = modeloTablaOrdenes.getValueAt(filaSeleccionada, 1).toString();
            if (estadoOrden.equalsIgnoreCase("ACTIVO")) {
                if (jcb_habilitar.isSelected()) {
                    long idOrden = (long) tbl_tablaOrdenes.getValueAt(filaSeleccionada, 0);
                    Orden orden = adaptadorOrden.buscarOrden(idOrden);

                    List<Detalle> lista = (List<Detalle>) orden.getDetalleColeccion();
                    for (Detalle detalle : lista) {
                        Object fila[] = new Object[]{
                            detalle.getId(),
                            detalle.getItem().getId(),
                            detalle.getItem().getNombre(),
                            detalle.getCantidad()
                        };
                        modeloTablaDetalle.addRow(fila);
                    }
                    jcb_llevar.setSelected(orden.isTipo());
                    btn_marcar.setEnabled(false);
                    btn_visualizar.setEnabled(false);
                    jcbox_Filtro.setEnabled(false);
                    jcbox_noMesa.setEnabled(false);

                    btn_añadir.setEnabled(true);
                    btn_eliminar.setEnabled(true);
                    btn_modificar.setEnabled(true);
                    jcbox_categoriaItems.setEnabled(true);
                    areaTextoInfoOrden.setText("");
                    tbl_tablaOrdenes.setEnabled(false);
                } else {

                    borrarDatosTabla(modeloTablaDetalle, tbl_tablaDetalleOrden);
                    btn_añadir.setEnabled(false);
                    btn_eliminar.setEnabled(false);
                    btn_modificar.setEnabled(false);
                    jcbox_categoriaItems.setSelectedIndex(0);
                    jcbox_categoriaItems.setEnabled(false);
                    areaTextoInfoOrden.setText("");

                    btn_marcar.setEnabled(true);
                    btn_visualizar.setEnabled(true);
                    jcbox_Filtro.setEnabled(true);
                    jcbox_noMesa.setEnabled(true);
                    tbl_tablaOrdenes.setEnabled(true);
                }
            } else {
                jcb_habilitar.setSelected(false);
                JOptionPane.showMessageDialog(null, "Solo se pueden modificar ordenes activas",
                        "Advertencia", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            jcb_habilitar.setSelected(false);
            JOptionPane.showMessageDialog(null, "Debe seleccionar primero una orden",
                    "Advertencia", JOptionPane.WARNING_MESSAGE);
        }


    }//GEN-LAST:event_jcb_habilitarActionPerformed

    private void jcbox_noMesaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jcbox_noMesaItemStateChanged
        borrarDatosTabla(modeloTablaOrdenes, tbl_tablaOrdenes);
        if (jcbox_noMesa.getSelectedIndex() > 0) {
            AdaptadorMesa adaptadorMesa = new AdaptadorMesa();
            String NoMesa = jcbox_noMesa.getSelectedItem().toString();
            Mesa mesa = (Mesa) adaptadorMesa.buscarMesa(NoMesa).get(0);
            //Se crea una lista con los objetos recuperados de la base de datos
            ArrayList lista;
            if (jcbox_Filtro.getSelectedIndex() > 0) {
                EstadoOrden estado = EstadoOrden.values()[jcbox_Filtro.getSelectedIndex() - 1];
                lista = new ArrayList(adaptadorOrden.listadoOrdenNoMesaEstado(mesa, estado));
            } else {
                lista = new ArrayList(adaptadorOrden.listadoOrdenes(mesa));
            }
            //Se hace un listado para poder usar el patron iterador propio
            Listado listado = new Listado(lista);
            for (Iterator iterator = listado.getIterator(); iterator.hasNext();) {
                Orden fila = (Orden) iterator.next();
                Object[] datos = new Object[4];
                datos[0] = fila.getId();
                datos[1] = fila.getEstado();
                datos[2] = fila.getMesa_orden().getNumero_mesa();
                datos[3] = fila.getInicio_servicio();
                modeloTablaOrdenes.addRow(datos);
            }
            adaptadorMesa.cerrarEntityManagerFactory();
        } else if (jcbox_Filtro.getSelectedIndex() > 0) {
            EstadoOrden estado = EstadoOrden.values()[jcbox_Filtro.getSelectedIndex() - 1];
            ArrayList lista = new ArrayList(adaptadorOrden.listadoOrdenes(estado));
            cargarTablaOrdenes(lista);
        }
    }//GEN-LAST:event_jcbox_noMesaItemStateChanged

    @Override
    public void update(Observable o, Object arg) {
        if (miObservador.isOrdenAñadida()) {
            Orden orden = adaptadorOrden.buscarOrden(miObservador.getIdOrden());
            Object fila[] = new Object[]{orden.getId(), orden.getEstado(),
                orden.getMesa_orden().getNumero_mesa(), orden.getInicio_servicio()};
            modeloTablaOrdenes.addRow(fila);
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea areaTextoInfoOrden;
    private javax.swing.JButton btn_añadir;
    private javax.swing.JButton btn_eliminar;
    private javax.swing.JButton btn_marcar;
    private javax.swing.JButton btn_modificar;
    private javax.swing.JButton btn_visualizar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JCheckBox jcb_habilitar;
    private javax.swing.JCheckBox jcb_llevar;
    private javax.swing.JComboBox<String> jcbox_Filtro;
    private javax.swing.JComboBox<String> jcbox_categoriaItems;
    private javax.swing.JComboBox<String> jcbox_noMesa;
    private javax.swing.JLabel lbl_Filtro;
    private javax.swing.JLabel lbl_busqueda;
    private javax.swing.JLabel lbl_titulo;
    private javax.swing.JPanel pnl_infoOrden;
    private javax.swing.JPanel pnl_listadoOrden;
    private javax.swing.JPanel pnl_principal;
    private javax.swing.JTable tbl_items;
    private javax.swing.JTable tbl_tablaDetalleOrden;
    private javax.swing.JTable tbl_tablaOrdenes;
    // End of variables declaration//GEN-END:variables
}
