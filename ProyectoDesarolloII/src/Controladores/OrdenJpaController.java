/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Controladores.exceptions.IllegalOrphanException;
import Controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelos.Empleado;
import Modelos.Mesa;
import Modelos.Detalle;
import Modelos.Orden;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author john
 */
public class OrdenJpaController implements Serializable {

    public OrdenJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Orden orden) {
        if (orden.getDetalleColeccion() == null) {
            orden.setDetalleColeccion(new ArrayList<Detalle>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Empleado mesero = orden.getMesero();
            if (mesero != null) {
                mesero = em.getReference(mesero.getClass(), mesero.getId());
                orden.setMesero(mesero);
            }
            Mesa mesa_orden = orden.getMesa_orden();
            if (mesa_orden != null) {
                mesa_orden = em.getReference(mesa_orden.getClass(), mesa_orden.getId());
                orden.setMesa_orden(mesa_orden);
            }
            Collection<Detalle> attachedDetalleColeccion = new ArrayList<Detalle>();
            for (Detalle detalleColeccionDetalleToAttach : orden.getDetalleColeccion()) {
                detalleColeccionDetalleToAttach = em.getReference(detalleColeccionDetalleToAttach.getClass(), detalleColeccionDetalleToAttach.getId());
                attachedDetalleColeccion.add(detalleColeccionDetalleToAttach);
            }
            orden.setDetalleColeccion(attachedDetalleColeccion);
            em.persist(orden);
            if (mesero != null) {
                mesero.getOrdenColeccion().add(orden);
                mesero = em.merge(mesero);
            }
            if (mesa_orden != null) {
                mesa_orden.getMesaColeccion().add(orden);
                mesa_orden = em.merge(mesa_orden);
            }
            for (Detalle detalleColeccionDetalle : orden.getDetalleColeccion()) {
                Orden oldOrdenOfDetalleColeccionDetalle = detalleColeccionDetalle.getOrden();
                detalleColeccionDetalle.setOrden(orden);
                detalleColeccionDetalle = em.merge(detalleColeccionDetalle);
                if (oldOrdenOfDetalleColeccionDetalle != null) {
                    oldOrdenOfDetalleColeccionDetalle.getDetalleColeccion().remove(detalleColeccionDetalle);
                    oldOrdenOfDetalleColeccionDetalle = em.merge(oldOrdenOfDetalleColeccionDetalle);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Orden orden) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Orden persistentOrden = em.find(Orden.class, orden.getId());
            Empleado meseroOld = persistentOrden.getMesero();
            Empleado meseroNew = orden.getMesero();
            Mesa mesa_ordenOld = persistentOrden.getMesa_orden();
            Mesa mesa_ordenNew = orden.getMesa_orden();
            Collection<Detalle> detalleColeccionOld = persistentOrden.getDetalleColeccion();
            Collection<Detalle> detalleColeccionNew = orden.getDetalleColeccion();
            List<String> illegalOrphanMessages = null;
            for (Detalle detalleColeccionOldDetalle : detalleColeccionOld) {
                if (!detalleColeccionNew.contains(detalleColeccionOldDetalle)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Detalle " + detalleColeccionOldDetalle + " since its orden field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (meseroNew != null) {
                meseroNew = em.getReference(meseroNew.getClass(), meseroNew.getId());
                orden.setMesero(meseroNew);
            }
            if (mesa_ordenNew != null) {
                mesa_ordenNew = em.getReference(mesa_ordenNew.getClass(), mesa_ordenNew.getId());
                orden.setMesa_orden(mesa_ordenNew);
            }
            Collection<Detalle> attachedDetalleColeccionNew = new ArrayList<Detalle>();
            for (Detalle detalleColeccionNewDetalleToAttach : detalleColeccionNew) {
                detalleColeccionNewDetalleToAttach = em.getReference(detalleColeccionNewDetalleToAttach.getClass(), detalleColeccionNewDetalleToAttach.getId());
                attachedDetalleColeccionNew.add(detalleColeccionNewDetalleToAttach);
            }
            detalleColeccionNew = attachedDetalleColeccionNew;
            orden.setDetalleColeccion(detalleColeccionNew);
            orden = em.merge(orden);
            if (meseroOld != null && !meseroOld.equals(meseroNew)) {
                meseroOld.getOrdenColeccion().remove(orden);
                meseroOld = em.merge(meseroOld);
            }
            if (meseroNew != null && !meseroNew.equals(meseroOld)) {
                meseroNew.getOrdenColeccion().add(orden);
                meseroNew = em.merge(meseroNew);
            }
            if (mesa_ordenOld != null && !mesa_ordenOld.equals(mesa_ordenNew)) {
                mesa_ordenOld.getMesaColeccion().remove(orden);
                mesa_ordenOld = em.merge(mesa_ordenOld);
            }
            if (mesa_ordenNew != null && !mesa_ordenNew.equals(mesa_ordenOld)) {
                mesa_ordenNew.getMesaColeccion().add(orden);
                mesa_ordenNew = em.merge(mesa_ordenNew);
            }
            for (Detalle detalleColeccionNewDetalle : detalleColeccionNew) {
                if (!detalleColeccionOld.contains(detalleColeccionNewDetalle)) {
                    Orden oldOrdenOfDetalleColeccionNewDetalle = detalleColeccionNewDetalle.getOrden();
                    detalleColeccionNewDetalle.setOrden(orden);
                    detalleColeccionNewDetalle = em.merge(detalleColeccionNewDetalle);
                    if (oldOrdenOfDetalleColeccionNewDetalle != null && !oldOrdenOfDetalleColeccionNewDetalle.equals(orden)) {
                        oldOrdenOfDetalleColeccionNewDetalle.getDetalleColeccion().remove(detalleColeccionNewDetalle);
                        oldOrdenOfDetalleColeccionNewDetalle = em.merge(oldOrdenOfDetalleColeccionNewDetalle);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = orden.getId();
                if (findOrden(id) == null) {
                    throw new NonexistentEntityException("The orden with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Orden orden;
            try {
                orden = em.getReference(Orden.class, id);
                orden.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The orden with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Detalle> detalleColeccionOrphanCheck = orden.getDetalleColeccion();
            for (Detalle detalleColeccionOrphanCheckDetalle : detalleColeccionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Orden (" + orden + ") cannot be destroyed since the Detalle " + detalleColeccionOrphanCheckDetalle + " in its detalleColeccion field has a non-nullable orden field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Empleado mesero = orden.getMesero();
            if (mesero != null) {
                mesero.getOrdenColeccion().remove(orden);
                mesero = em.merge(mesero);
            }
            Mesa mesa_orden = orden.getMesa_orden();
            if (mesa_orden != null) {
                mesa_orden.getMesaColeccion().remove(orden);
                mesa_orden = em.merge(mesa_orden);
            }
            em.remove(orden);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Orden> findOrdenEntities() {
        return findOrdenEntities(true, -1, -1);
    }

    public List<Orden> findOrdenEntities(int maxResults, int firstResult) {
        return findOrdenEntities(false, maxResults, firstResult);
    }

    private List<Orden> findOrdenEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Orden.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Orden findOrden(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Orden.class, id);
        } finally {
            em.close();
        }
    }

    public int getOrdenCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Orden> rt = cq.from(Orden.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
