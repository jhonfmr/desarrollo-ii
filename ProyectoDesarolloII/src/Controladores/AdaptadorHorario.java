
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Controladores.Abstractas.AccesoBdHorario;
import Controladores.exceptions.NonexistentEntityException;
import Modelos.Horario;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.JOptionPane;

/**
 *
 * @author invitado
 */
public class AdaptadorHorario extends AccesoBdHorario {
    
    private EntityManagerFactory emf;
    
    public AdaptadorHorario() {
        this.emf = Persistence.createEntityManagerFactory("Persistencia");
    }
    
    @Override
    public void cerrarEntityManagerFactory() {
        emf.close();
    }

    @Override
    public void almacenarHorario(Horario horario) {
        HorarioJpaController adaptado = new HorarioJpaController(emf);
        adaptado.create(horario);
    }

    @Override
    public Horario buscarHorario(long id) {
        HorarioJpaController adaptado = new HorarioJpaController(emf);
        return adaptado.findHorario(id);
    }

    @Override
    public void eliminarHorario(Horario horario) {
        EntityManager manejador = null;
        try {
            manejador = emf.createEntityManager();
            manejador.getTransaction().begin();
            Horario detalle = manejador.getReference(Horario.class, horario.getId());
            manejador.remove(detalle);
            manejador.getTransaction().commit();
        } finally {
            if (manejador == null) {
                manejador.close();
            }
        }
    }

       
}
