/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Controladores.exceptions.IllegalOrphanException;
import Controladores.exceptions.NonexistentEntityException;
import Modelos.Empleado;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelos.Orden;
import java.util.ArrayList;
import java.util.Collection;
import Modelos.Factura;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author john
 */
public class EmpleadoJpaController implements Serializable {

    public EmpleadoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Empleado empleado) {
        if (empleado.getOrdenColeccion() == null) {
            empleado.setOrdenColeccion(new ArrayList<Orden>());
        }
        if (empleado.getFacturaColeccion() == null) {
            empleado.setFacturaColeccion(new ArrayList<Factura>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Orden> attachedOrdenColeccion = new ArrayList<Orden>();
            for (Orden ordenColeccionOrdenToAttach : empleado.getOrdenColeccion()) {
                ordenColeccionOrdenToAttach = em.getReference(ordenColeccionOrdenToAttach.getClass(), ordenColeccionOrdenToAttach.getId());
                attachedOrdenColeccion.add(ordenColeccionOrdenToAttach);
            }
            empleado.setOrdenColeccion(attachedOrdenColeccion);
            Collection<Factura> attachedFacturaColeccion = new ArrayList<Factura>();
            for (Factura facturaColeccionFacturaToAttach : empleado.getFacturaColeccion()) {
                facturaColeccionFacturaToAttach = em.getReference(facturaColeccionFacturaToAttach.getClass(), facturaColeccionFacturaToAttach.getNoFactura());
                attachedFacturaColeccion.add(facturaColeccionFacturaToAttach);
            }
            empleado.setFacturaColeccion(attachedFacturaColeccion);
            em.persist(empleado);
            for (Orden ordenColeccionOrden : empleado.getOrdenColeccion()) {
                Empleado oldMeseroOfOrdenColeccionOrden = ordenColeccionOrden.getMesero();
                ordenColeccionOrden.setMesero(empleado);
                ordenColeccionOrden = em.merge(ordenColeccionOrden);
                if (oldMeseroOfOrdenColeccionOrden != null) {
                    oldMeseroOfOrdenColeccionOrden.getOrdenColeccion().remove(ordenColeccionOrden);
                    oldMeseroOfOrdenColeccionOrden = em.merge(oldMeseroOfOrdenColeccionOrden);
                }
            }
            for (Factura facturaColeccionFactura : empleado.getFacturaColeccion()) {
                Empleado oldCajeroOfFacturaColeccionFactura = facturaColeccionFactura.getCajero();
                facturaColeccionFactura.setCajero(empleado);
                facturaColeccionFactura = em.merge(facturaColeccionFactura);
                if (oldCajeroOfFacturaColeccionFactura != null) {
                    oldCajeroOfFacturaColeccionFactura.getFacturaColeccion().remove(facturaColeccionFactura);
                    oldCajeroOfFacturaColeccionFactura = em.merge(oldCajeroOfFacturaColeccionFactura);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Empleado empleado) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Empleado persistentEmpleado = em.find(Empleado.class, empleado.getId());
            Collection<Orden> ordenColeccionOld = persistentEmpleado.getOrdenColeccion();
            Collection<Orden> ordenColeccionNew = empleado.getOrdenColeccion();
            Collection<Factura> facturaColeccionOld = persistentEmpleado.getFacturaColeccion();
            Collection<Factura> facturaColeccionNew = empleado.getFacturaColeccion();
            List<String> illegalOrphanMessages = null;
            for (Orden ordenColeccionOldOrden : ordenColeccionOld) {
                if (!ordenColeccionNew.contains(ordenColeccionOldOrden)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Orden " + ordenColeccionOldOrden + " since its mesero field is not nullable.");
                }
            }
            for (Factura facturaColeccionOldFactura : facturaColeccionOld) {
                if (!facturaColeccionNew.contains(facturaColeccionOldFactura)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Factura " + facturaColeccionOldFactura + " since its cajero field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Orden> attachedOrdenColeccionNew = new ArrayList<Orden>();
            for (Orden ordenColeccionNewOrdenToAttach : ordenColeccionNew) {
                ordenColeccionNewOrdenToAttach = em.getReference(ordenColeccionNewOrdenToAttach.getClass(), ordenColeccionNewOrdenToAttach.getId());
                attachedOrdenColeccionNew.add(ordenColeccionNewOrdenToAttach);
            }
            ordenColeccionNew = attachedOrdenColeccionNew;
            empleado.setOrdenColeccion(ordenColeccionNew);
            Collection<Factura> attachedFacturaColeccionNew = new ArrayList<Factura>();
            for (Factura facturaColeccionNewFacturaToAttach : facturaColeccionNew) {
                facturaColeccionNewFacturaToAttach = em.getReference(facturaColeccionNewFacturaToAttach.getClass(), facturaColeccionNewFacturaToAttach.getNoFactura());
                attachedFacturaColeccionNew.add(facturaColeccionNewFacturaToAttach);
            }
            facturaColeccionNew = attachedFacturaColeccionNew;
            empleado.setFacturaColeccion(facturaColeccionNew);
            empleado = em.merge(empleado);
            for (Orden ordenColeccionNewOrden : ordenColeccionNew) {
                if (!ordenColeccionOld.contains(ordenColeccionNewOrden)) {
                    Empleado oldMeseroOfOrdenColeccionNewOrden = ordenColeccionNewOrden.getMesero();
                    ordenColeccionNewOrden.setMesero(empleado);
                    ordenColeccionNewOrden = em.merge(ordenColeccionNewOrden);
                    if (oldMeseroOfOrdenColeccionNewOrden != null && !oldMeseroOfOrdenColeccionNewOrden.equals(empleado)) {
                        oldMeseroOfOrdenColeccionNewOrden.getOrdenColeccion().remove(ordenColeccionNewOrden);
                        oldMeseroOfOrdenColeccionNewOrden = em.merge(oldMeseroOfOrdenColeccionNewOrden);
                    }
                }
            }
            for (Factura facturaColeccionNewFactura : facturaColeccionNew) {
                if (!facturaColeccionOld.contains(facturaColeccionNewFactura)) {
                    Empleado oldCajeroOfFacturaColeccionNewFactura = facturaColeccionNewFactura.getCajero();
                    facturaColeccionNewFactura.setCajero(empleado);
                    facturaColeccionNewFactura = em.merge(facturaColeccionNewFactura);
                    if (oldCajeroOfFacturaColeccionNewFactura != null && !oldCajeroOfFacturaColeccionNewFactura.equals(empleado)) {
                        oldCajeroOfFacturaColeccionNewFactura.getFacturaColeccion().remove(facturaColeccionNewFactura);
                        oldCajeroOfFacturaColeccionNewFactura = em.merge(oldCajeroOfFacturaColeccionNewFactura);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = empleado.getId();
                if (findEmpleado(id) == null) {
                    throw new NonexistentEntityException("The empleado with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Empleado empleado;
            try {
                empleado = em.getReference(Empleado.class, id);
                empleado.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The empleado with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Orden> ordenColeccionOrphanCheck = empleado.getOrdenColeccion();
            for (Orden ordenColeccionOrphanCheckOrden : ordenColeccionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Empleado (" + empleado + ") cannot be destroyed since the Orden " + ordenColeccionOrphanCheckOrden + " in its ordenColeccion field has a non-nullable mesero field.");
            }
            Collection<Factura> facturaColeccionOrphanCheck = empleado.getFacturaColeccion();
            for (Factura facturaColeccionOrphanCheckFactura : facturaColeccionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Empleado (" + empleado + ") cannot be destroyed since the Factura " + facturaColeccionOrphanCheckFactura + " in its facturaColeccion field has a non-nullable cajero field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(empleado);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Empleado> findEmpleadoEntities() {
        return findEmpleadoEntities(true, -1, -1);
    }

    public List<Empleado> findEmpleadoEntities(int maxResults, int firstResult) {
        return findEmpleadoEntities(false, maxResults, firstResult);
    }

    private List<Empleado> findEmpleadoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Empleado.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Empleado findEmpleado(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Empleado.class, id);
        } finally {
            em.close();
        }
    }

    public int getEmpleadoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Empleado> rt = cq.from(Empleado.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
