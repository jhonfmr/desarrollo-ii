
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Controladores.Abstractas.AccesoBdOrden;
import Controladores.exceptions.NonexistentEntityException;
import Modelos.Mesa;
import Modelos.Orden;
import Modelos.Orden.EstadoOrden;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.JOptionPane;

/**
 *
 * @author invitado
 */
public class AdaptadorOrden extends AccesoBdOrden {

    private EntityManagerFactory emf;

    public AdaptadorOrden() {
        this.emf = Persistence.createEntityManagerFactory("Persistencia");
    }

    @Override
    public void cerrarEntityManagerFactory() {
        emf.close();
    }

    @Override
    public List listadoOrdenes() {
        OrdenJpaController adaptada = new OrdenJpaController(emf);
        return adaptada.findOrdenEntities();
    }

    @Override
    public List listadoOrdenes(EstadoOrden estado) {
        EntityManager manejador = null;
        try {
            manejador = emf.createEntityManager();
            manejador.getTransaction().begin();
            Query query = manejador.createQuery("FROM Orden WHERE estado = :estado");
            query.setParameter("estado", estado);
            manejador.getTransaction().commit();
            return query.getResultList();
        } finally {
            if (manejador == null) {
                manejador.close();
            }
        }

    }

    @Override
    public void almacenarOrden(Orden orden) {
        OrdenJpaController adaptada = new OrdenJpaController(emf);
        adaptada.create(orden);
    }

    @Override
    public Orden buscarOrden(long id) {
        OrdenJpaController adaptado = new OrdenJpaController(emf);
        return adaptado.findOrden(id);
    }

    @Override
    public List buscarOrdenEstado(String mesa,String estado) {
        EntityManager manejador = null;
        try {
            manejador = emf.createEntityManager();
            Query query = manejador.createQuery("FROM Orden WHERE mesa_orden_id =:num_mesa AND estado_orden =:estado");
            query.setParameter("num_mesa", Integer.parseInt(mesa));
            query.setParameter("estado", estado);
            return query.getResultList();
        } finally {
            if (manejador == null) {
                manejador.close();
            }
        }
    }

    @Override
    public boolean editarOrden(Orden orden) {
        OrdenJpaController adaptado = new OrdenJpaController(emf);
        boolean exito = false;
        try {
            adaptado.edit(orden);
            exito = true;
        } catch (NonexistentEntityException ex) {
            JOptionPane.showMessageDialog(null, "Error con la entidad de tipo: " + ex,
                    "Error", JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error tipo: " + ex,
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return exito;
    }

    @Override
    public List listadoOrdenNoMesaEstado(Mesa mesa, EstadoOrden estado) {
        EntityManager manejador = null;
        try {
            manejador = emf.createEntityManager();
            manejador.getTransaction().begin();
            Query query;
            query = manejador.createQuery("FROM Orden WHERE mesa_orden =:num_mesa AND estado=:estadoMesa");
            query.setParameter("num_mesa", mesa);
            query.setParameter("estadoMesa", estado);
            manejador.getTransaction().commit();
            return query.getResultList();
        } finally {
            if (manejador == null) {
                manejador.close();
            }
        }
    }

    @Override
    public List listadoOrdenes(Mesa mesa) {
        EntityManager manejador = null;
        try {
            manejador = emf.createEntityManager();
            manejador.getTransaction().begin();
            Query query = manejador.createQuery("FROM Orden WHERE mesa_orden = :mesa");
            query.setParameter("mesa", mesa);
            manejador.getTransaction().commit();
            return query.getResultList();
        } finally {
            if (manejador == null) {
                manejador.close();
            }
        }
    }

}
