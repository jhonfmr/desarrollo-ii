/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Controladores.exceptions.IllegalOrphanException;
import Controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelos.Detalle;
import Modelos.Item;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author john
 */
public class ItemJpaController implements Serializable {

    public ItemJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Item item) {
        if (item.getDetalleColeccion() == null) {
            item.setDetalleColeccion(new ArrayList<Detalle>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Detalle> attachedDetalleColeccion = new ArrayList<Detalle>();
            for (Detalle detalleColeccionDetalleToAttach : item.getDetalleColeccion()) {
                detalleColeccionDetalleToAttach = em.getReference(detalleColeccionDetalleToAttach.getClass(), detalleColeccionDetalleToAttach.getId());
                attachedDetalleColeccion.add(detalleColeccionDetalleToAttach);
            }
            item.setDetalleColeccion(attachedDetalleColeccion);
            em.persist(item);
            for (Detalle detalleColeccionDetalle : item.getDetalleColeccion()) {
                Item oldItemOfDetalleColeccionDetalle = detalleColeccionDetalle.getItem();
                detalleColeccionDetalle.setItem(item);
                detalleColeccionDetalle = em.merge(detalleColeccionDetalle);
                if (oldItemOfDetalleColeccionDetalle != null) {
                    oldItemOfDetalleColeccionDetalle.getDetalleColeccion().remove(detalleColeccionDetalle);
                    oldItemOfDetalleColeccionDetalle = em.merge(oldItemOfDetalleColeccionDetalle);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Item item) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Item persistentItem = em.find(Item.class, item.getId());
            Collection<Detalle> detalleColeccionOld = persistentItem.getDetalleColeccion();
            Collection<Detalle> detalleColeccionNew = item.getDetalleColeccion();
            List<String> illegalOrphanMessages = null;
            for (Detalle detalleColeccionOldDetalle : detalleColeccionOld) {
                if (!detalleColeccionNew.contains(detalleColeccionOldDetalle)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Detalle " + detalleColeccionOldDetalle + " since its item field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Detalle> attachedDetalleColeccionNew = new ArrayList<Detalle>();
            for (Detalle detalleColeccionNewDetalleToAttach : detalleColeccionNew) {
                detalleColeccionNewDetalleToAttach = em.getReference(detalleColeccionNewDetalleToAttach.getClass(), detalleColeccionNewDetalleToAttach.getId());
                attachedDetalleColeccionNew.add(detalleColeccionNewDetalleToAttach);
            }
            detalleColeccionNew = attachedDetalleColeccionNew;
            item.setDetalleColeccion(detalleColeccionNew);
            item = em.merge(item);
            for (Detalle detalleColeccionNewDetalle : detalleColeccionNew) {
                if (!detalleColeccionOld.contains(detalleColeccionNewDetalle)) {
                    Item oldItemOfDetalleColeccionNewDetalle = detalleColeccionNewDetalle.getItem();
                    detalleColeccionNewDetalle.setItem(item);
                    detalleColeccionNewDetalle = em.merge(detalleColeccionNewDetalle);
                    if (oldItemOfDetalleColeccionNewDetalle != null && !oldItemOfDetalleColeccionNewDetalle.equals(item)) {
                        oldItemOfDetalleColeccionNewDetalle.getDetalleColeccion().remove(detalleColeccionNewDetalle);
                        oldItemOfDetalleColeccionNewDetalle = em.merge(oldItemOfDetalleColeccionNewDetalle);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = item.getId();
                if (findItem(id) == null) {
                    throw new NonexistentEntityException("The item with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Item item;
            try {
                item = em.getReference(Item.class, id);
                item.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The item with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Detalle> detalleColeccionOrphanCheck = item.getDetalleColeccion();
            for (Detalle detalleColeccionOrphanCheckDetalle : detalleColeccionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Item (" + item + ") cannot be destroyed since the Detalle " + detalleColeccionOrphanCheckDetalle + " in its detalleColeccion field has a non-nullable item field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(item);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Item> findItemEntities() {
        return findItemEntities(true, -1, -1);
    }

    public List<Item> findItemEntities(int maxResults, int firstResult) {
        return findItemEntities(false, maxResults, firstResult);
    }

    private List<Item> findItemEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Item.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Item findItem(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Item.class, id);
        } finally {
            em.close();
        }
    }

    public int getItemCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Item> rt = cq.from(Item.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
