
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Controladores.Abstractas.AccesoBdItems;
import Controladores.exceptions.NonexistentEntityException;
import Modelos.Item;
import Modelos.Orden;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.JOptionPane;

/**
 *
 * @author invitado
 */
public class AdaptadorItem extends AccesoBdItems {

    private EntityManagerFactory emf;

    public AdaptadorItem() {
        this.emf = Persistence.createEntityManagerFactory("Persistencia");
    }

    @Override
    public void cerrarEntityManagerFactory() {
        emf.close();
    }
    
    @Override
    public Item buscarItem(long id){
        ItemJpaController adaptado = new ItemJpaController(emf);
        return adaptado.findItem(id);
    }

    @Override
    public List listadoItems() {
        ItemJpaController adaptada = new ItemJpaController(emf);
        return adaptada.findItemEntities();
    }

    @Override
    public List ListadoItems(String categoria) {
        EntityManager manejador = null;
        try {
            manejador = emf.createEntityManager();
            Query query = manejador.createQuery("FROM Item WHERE categoria_item = :categoria");
            query.setParameter("categoria", categoria);
            return query.getResultList();
        } finally {
            if (manejador == null) {
                manejador.close();
            }
        }
    }
     
    
    public List buscarItemPorNombre(String nombre) {
        EntityManager manejador = null;
        try {
            manejador = emf.createEntityManager();
            Query query = manejador.createQuery("FROM Item WHERE nombre_item = :nombre");
            query.setParameter("nombre", nombre);
            return query.getResultList();
        } finally {
            if (manejador == null) {
                manejador.close();
            }
        }
    }
    @Override
    public boolean modificarItem(Item item) {
        ItemJpaController adaptado = new ItemJpaController(emf);
        boolean exito = false;
        try {
            adaptado.edit(item);
            exito = true;
        } catch (NonexistentEntityException ex) {
            JOptionPane.showMessageDialog(null, "Error con la entidad de tipo: " + ex,
                    "Error", JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error tipo: " + ex,
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return exito;
    }

    @Override
    public void almacenarItem(Item item) {
        ItemJpaController adaptado = new ItemJpaController(emf);
        adaptado.create(item);
    }

}
