/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Controladores.exceptions.IllegalOrphanException;
import Controladores.exceptions.NonexistentEntityException;
import Modelos.Mesa;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelos.Orden;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author john
 */
public class MesaJpaController implements Serializable {

    public MesaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Mesa mesa) {
        if (mesa.getMesaColeccion() == null) {
            mesa.setMesaColeccion(new ArrayList<Orden>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Orden> attachedMesaColeccion = new ArrayList<Orden>();
            for (Orden mesaColeccionOrdenToAttach : mesa.getMesaColeccion()) {
                mesaColeccionOrdenToAttach = em.getReference(mesaColeccionOrdenToAttach.getClass(), mesaColeccionOrdenToAttach.getId());
                attachedMesaColeccion.add(mesaColeccionOrdenToAttach);
            }
            mesa.setMesaColeccion(attachedMesaColeccion);
            em.persist(mesa);
            for (Orden mesaColeccionOrden : mesa.getMesaColeccion()) {
                Mesa oldMesa_ordenOfMesaColeccionOrden = mesaColeccionOrden.getMesa_orden();
                mesaColeccionOrden.setMesa_orden(mesa);
                mesaColeccionOrden = em.merge(mesaColeccionOrden);
                if (oldMesa_ordenOfMesaColeccionOrden != null) {
                    oldMesa_ordenOfMesaColeccionOrden.getMesaColeccion().remove(mesaColeccionOrden);
                    oldMesa_ordenOfMesaColeccionOrden = em.merge(oldMesa_ordenOfMesaColeccionOrden);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Mesa mesa) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Mesa persistentMesa = em.find(Mesa.class, mesa.getId());
            Collection<Orden> mesaColeccionOld = persistentMesa.getMesaColeccion();
            Collection<Orden> mesaColeccionNew = mesa.getMesaColeccion();
            List<String> illegalOrphanMessages = null;
            for (Orden mesaColeccionOldOrden : mesaColeccionOld) {
                if (!mesaColeccionNew.contains(mesaColeccionOldOrden)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Orden " + mesaColeccionOldOrden + " since its mesa_orden field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Orden> attachedMesaColeccionNew = new ArrayList<Orden>();
            for (Orden mesaColeccionNewOrdenToAttach : mesaColeccionNew) {
                mesaColeccionNewOrdenToAttach = em.getReference(mesaColeccionNewOrdenToAttach.getClass(), mesaColeccionNewOrdenToAttach.getId());
                attachedMesaColeccionNew.add(mesaColeccionNewOrdenToAttach);
            }
            mesaColeccionNew = attachedMesaColeccionNew;
            mesa.setMesaColeccion(mesaColeccionNew);
            mesa = em.merge(mesa);
            for (Orden mesaColeccionNewOrden : mesaColeccionNew) {
                if (!mesaColeccionOld.contains(mesaColeccionNewOrden)) {
                    Mesa oldMesa_ordenOfMesaColeccionNewOrden = mesaColeccionNewOrden.getMesa_orden();
                    mesaColeccionNewOrden.setMesa_orden(mesa);
                    mesaColeccionNewOrden = em.merge(mesaColeccionNewOrden);
                    if (oldMesa_ordenOfMesaColeccionNewOrden != null && !oldMesa_ordenOfMesaColeccionNewOrden.equals(mesa)) {
                        oldMesa_ordenOfMesaColeccionNewOrden.getMesaColeccion().remove(mesaColeccionNewOrden);
                        oldMesa_ordenOfMesaColeccionNewOrden = em.merge(oldMesa_ordenOfMesaColeccionNewOrden);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = mesa.getId();
                if (findMesa(id) == null) {
                    throw new NonexistentEntityException("The mesa with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Mesa mesa;
            try {
                mesa = em.getReference(Mesa.class, id);
                mesa.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The mesa with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Orden> mesaColeccionOrphanCheck = mesa.getMesaColeccion();
            for (Orden mesaColeccionOrphanCheckOrden : mesaColeccionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Mesa (" + mesa + ") cannot be destroyed since the Orden " + mesaColeccionOrphanCheckOrden + " in its mesaColeccion field has a non-nullable mesa_orden field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(mesa);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Mesa> findMesaEntities() {
        return findMesaEntities(true, -1, -1);
    }

    public List<Mesa> findMesaEntities(int maxResults, int firstResult) {
        return findMesaEntities(false, maxResults, firstResult);
    }

    private List<Mesa> findMesaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Mesa.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Mesa findMesa(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Mesa.class, id);
        } finally {
            em.close();
        }
    }

    public int getMesaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Mesa> rt = cq.from(Mesa.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
