
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Controladores.Abstractas.AccesoBdDetalleFactura;
import Modelos.Detalle;
import Modelos.DetalleFactura;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author invitado
 */
public class AdaptadorDetalleFactura extends AccesoBdDetalleFactura {

    private EntityManagerFactory emf;

    public AdaptadorDetalleFactura() {
        this.emf = Persistence.createEntityManagerFactory("Persistencia");
    }

    @Override
    public void cerrarEntityManagerFactory() {
        emf.close();
    }

    @Override
    public void almacenarDetalleFactura(DetalleFactura detalleFactura) {
        DetalleFacturaJpaController adaptado = new DetalleFacturaJpaController(emf);
        adaptado.create(detalleFactura);
    }

    @Override
    public void EliminarDetalleFactura(Detalle detalleFactura) {
        EntityManager manejador = null;
        try {
            manejador = emf.createEntityManager();
            manejador.getTransaction().begin();
            Detalle detalle = manejador.getReference(Detalle.class, detalleFactura.getId());
            manejador.remove(detalle);
            manejador.getTransaction().commit();
        } finally {
            if (manejador == null) {
                manejador.close();
            }
        }
    }

    @Override
    public Detalle buscarDetalleFactura(long id) {
        DetalleJpaController adaptado = new DetalleJpaController(emf);
        return adaptado.findDetalle(id);
    }

}
