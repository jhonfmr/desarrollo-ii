/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores.Abstractas;

import Modelos.Horario;
import java.util.List;

/**
 *
 * @author john
 */
public abstract class AccesoBdHorario {
    
    public abstract void cerrarEntityManagerFactory();
    public abstract void almacenarHorario(Horario horario);
    public abstract Horario buscarHorario(long id);
    public abstract void eliminarHorario(Horario horario);
   
}
