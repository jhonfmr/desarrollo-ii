/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores.Abstractas;

import Modelos.Factura;
import java.util.List;

/**
 *
 * @author john
 */
public abstract class AccesoBdFactura {
    
    public abstract void cerrarEntityManagerFactory();
    public abstract void almacenarFactura(Factura pago);
    public abstract Factura buscarFactura(long id);
    public abstract List listarPagosFactura(int factura);
    public abstract boolean editarFactura(Factura factura);
    public abstract List buscarPorNoFactura(String factura);
    public abstract List listadoFacturas();
    
}
