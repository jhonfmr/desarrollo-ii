/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores.Abstractas;

import Modelos.Empleado;
import java.util.List;

/**
 *
 * @author john
 */
public abstract class AccesoBdEmpleado {
    
    public abstract void cerrarEntityManagerFactory();
    public abstract void almacenarEmpleado(Empleado empleado);
    public abstract Empleado buscarEmpleado(long id);
    public abstract List listadoEmpleado();
    public abstract List buscarEmpleadoConCedula(String cedula);
    public abstract List buscarEmpleadoConCargo(String cargo);
    public abstract boolean editarEmpleado(Empleado empleado);
    public abstract List recuperarCargo(String usuario, String password); 
}
