/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores.Abstractas;

import Modelos.Pago;
import java.util.List;

/**
 *
 * @author john
 */
public abstract class AccesoBdPago {
    
    public abstract void cerrarEntityManagerFactory();
    public abstract void almacenarPago(Pago pago);
    public abstract Pago buscarPago(long id);
    public abstract List buscarPagosNoFactura(String factura);
    public abstract boolean editarOrden(Pago pago);
    public abstract List listadoPagos();
    
}
