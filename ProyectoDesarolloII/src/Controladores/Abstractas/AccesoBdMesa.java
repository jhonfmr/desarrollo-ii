/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores.Abstractas;

import Modelos.Mesa;
import java.util.List;

/**
 *
 * @author jhonfmr
 */
public abstract class AccesoBdMesa  {
    public abstract List listadoMesas();
    public abstract void cerrarEntityManagerFactory();
    public abstract void almacenarMesa(Mesa mesa);
    public abstract boolean editarMesa(Mesa mesa);
    public abstract Mesa buscarMesa(long id);
    public abstract List buscarMesa(String numero);
}
