/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores.Abstractas;

import Modelos.Detalle;

/**
 *
 * @author john
 */
public abstract class AccesoBdDetalle {
    
    public abstract void cerrarEntityManagerFactory();
    public abstract void almacenarDetalleOrden(Detalle detalleOrden);
    public abstract Detalle buscarDetalleOrden(long id);
    public abstract void EliminarDetalle(Detalle detalleOrden);
    public abstract boolean editarDetalle(Detalle detalleOrden);
}
