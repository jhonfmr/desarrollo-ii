/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores.Abstractas;

import Modelos.Detalle;
import Modelos.DetalleFactura;

/**
 *
 * @author john
 */
public abstract class AccesoBdDetalleFactura {
    
    public abstract void cerrarEntityManagerFactory();
    public abstract void almacenarDetalleFactura(DetalleFactura detalleFactura);
    public abstract Detalle buscarDetalleFactura(long id);
    public abstract void EliminarDetalleFactura(Detalle detalleOrden);
}
