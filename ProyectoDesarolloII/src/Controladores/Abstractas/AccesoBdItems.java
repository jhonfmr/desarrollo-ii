/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores.Abstractas;

import Modelos.Item;
import java.util.List;

/**
 *
 * @author jhonfmr
 */
public abstract class AccesoBdItems {
    
    public abstract void almacenarItem(Item item);
    public abstract Item buscarItem(long id);
    public abstract List listadoItems();
    public abstract List ListadoItems(String categoria);
    public abstract void cerrarEntityManagerFactory();
    public abstract boolean modificarItem(Item item);
    
}
