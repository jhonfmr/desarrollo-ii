/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores.Abstractas;

import Modelos.Mesa;
import Modelos.Orden;
import Modelos.Orden.EstadoOrden;
import java.util.List;

/**
 *
 * @author jhonfmr
 */
public abstract class AccesoBdOrden {
    public abstract List listadoOrdenes();
    public abstract List listadoOrdenes(EstadoOrden estado);
    public abstract List listadoOrdenes(Mesa mesa);
    public abstract List listadoOrdenNoMesaEstado(Mesa mesa,EstadoOrden estado);
    public abstract void cerrarEntityManagerFactory();
    public abstract void almacenarOrden(Orden orden);
    public abstract Orden buscarOrden(long id);
    public abstract boolean editarOrden(Orden orden);
    public abstract List buscarOrdenEstado(String mesa,String estado);
    
}
