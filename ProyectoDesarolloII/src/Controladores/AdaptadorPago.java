
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Controladores.Abstractas.AccesoBdPago;
import Controladores.exceptions.NonexistentEntityException;
import Modelos.Factura;
import Modelos.Pago;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.JOptionPane;

/**
 *
 * @author invitado
 */
public class AdaptadorPago extends AccesoBdPago {
    
    private EntityManagerFactory emf;
    
    public AdaptadorPago() {
        this.emf = Persistence.createEntityManagerFactory("Persistencia");
    }
    
    @Override
    public void almacenarPago(Pago pago){
        PagoJpaController adaptado = new PagoJpaController(emf);
        adaptado.create(pago);
    }
    
    @Override
    public Pago buscarPago(long id){
        PagoJpaController adaptado = new PagoJpaController(emf);
        return adaptado.findPago(id);
    }
    
    @Override
    public List buscarPagosNoFactura(String factura){
    EntityManager manejador = null;  
    int num_factura = Integer.parseInt(factura.trim());
       try{
       manejador = emf.createEntityManager();
       Query query = manejador.createQuery("FROM Pago WHERE nofactura =:num_factura");
       query.setParameter("num_factura", num_factura);      
       return query.getResultList();
       } finally{
       
           if(manejador == null){
               manejador.close();
           
           }
       
       }
    
    
    }
    
    @Override
    public void cerrarEntityManagerFactory() {
        emf.close();
    }
    
    @Override
    public boolean editarOrden(Pago pago) {
        PagoJpaController adaptado = new PagoJpaController(emf);
        boolean exito = false;
        try {
            adaptado.edit(pago);
            exito = true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error tipo: " + ex,
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return exito;
    }


    @Override
    public List listadoPagos() {
        PagoJpaController adaptado = new PagoJpaController(emf);
        return adaptado.findPagoEntities();
    }
    
    
}
