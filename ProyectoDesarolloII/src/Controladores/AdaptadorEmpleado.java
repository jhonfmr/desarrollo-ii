
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Controladores.Abstractas.AccesoBdEmpleado;
import Controladores.exceptions.NonexistentEntityException;
import Modelos.Empleado;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.JOptionPane;

/**
 *
 * @author invitado
 */
public class AdaptadorEmpleado extends AccesoBdEmpleado {

    private EntityManagerFactory emf;

    public AdaptadorEmpleado() {
        this.emf = Persistence.createEntityManagerFactory("Persistencia");
    }

    @Override
    public void cerrarEntityManagerFactory() {
        emf.close();
    }

    @Override
    public void almacenarEmpleado(Empleado emp) {
        EmpleadoJpaController adaptado = new EmpleadoJpaController(emf);
        adaptado.create(emp);
    }

    @Override
    public Empleado buscarEmpleado(long id) {
        EmpleadoJpaController adaptado = new EmpleadoJpaController(emf);
        return adaptado.findEmpleado(id);
    }

    @Override
    public List buscarEmpleadoConCedula(String cedula) {
        EntityManager manejador = null;
        try {
            manejador = emf.createEntityManager();
            Query query = manejador.createQuery("FROM Empleado WHERE numero_identificacion =:cedula");
            query.setParameter("cedula", cedula);
            return query.getResultList();
        } finally {

            if (manejador == null) {
                manejador.close();

            }

        }
    }

    @Override
    public List listadoEmpleado() {
        EntityManager manejador = null;
        try {
            manejador = emf.createEntityManager();
            Query query = manejador.createQuery("FROM Empleado");

            return query.getResultList();
        } finally {

            if (manejador == null) {
                manejador.close();

            }

        }
    }

    @Override
    public List buscarEmpleadoConCargo(String cargo) {
        EntityManager manejador = null;
        try {
            manejador = emf.createEntityManager();
            Query query = manejador.createQuery("FROM Empleado WHERE cargo_empleado =:cargo");
            query.setParameter("cargo", cargo);
            return query.getResultList();
        } finally {

            if (manejador == null) {
                manejador.close();

            }

        }
    }

    @Override
    public boolean editarEmpleado(Empleado empleado) {
        EmpleadoJpaController adaptado = new EmpleadoJpaController(emf);
        boolean exito = false;
        try {
            adaptado.edit(empleado);
            exito = true;
        } catch (NonexistentEntityException ex) {
            JOptionPane.showMessageDialog(null, "Error con la entidad de tipo: " + ex,
                    "Error", JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error tipo: " + ex,
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return exito;
    }

    @Override
    public List recuperarCargo(String usuario, String password) {
        EntityManager manejador = null;
        try {
            manejador = emf.createEntityManager();
            Query query = manejador.createQuery("FROM Empleado WHERE contraseña =:password AND usuario =:usuario");
            query.setParameter("password", password);
            query.setParameter("usuario", usuario);
            return query.getResultList();
        } finally {

            if (manejador == null) {
                manejador.close();

            }

        }
    }

}
