
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Controladores.Abstractas.AccesoBdFactura;
import Controladores.Abstractas.AccesoBdPago;
import Controladores.exceptions.NonexistentEntityException;
import Modelos.Factura;
import Modelos.Pago;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.JOptionPane;

/**
 *
 * @author invitado
 */
public class AdaptadorFactura extends AccesoBdFactura {

    private EntityManagerFactory emf;

    public AdaptadorFactura() {
        this.emf = Persistence.createEntityManagerFactory("Persistencia");
    }

    @Override
    public void almacenarFactura(Factura factura) {
        FacturaJpaController adaptado = new FacturaJpaController(emf);
        adaptado.create(factura);
    }

    @Override
    public Factura buscarFactura(long id) {
        FacturaJpaController adaptado = new FacturaJpaController(emf);
        return adaptado.findFactura(id);
    }

    @Override
    public List buscarPorNoFactura(String factura) {
        EntityManager manejador = null;
        int num_factura = Integer.parseInt(factura.trim());
        try {
            manejador = emf.createEntityManager();
            Query query = manejador.createQuery("FROM Factura WHERE nofactura =:num_factura");
            query.setParameter("num_factura", num_factura);
            return query.getResultList();
        } finally {

            if (manejador == null) {
                manejador.close();

            }

        }

    }

    @Override
    public void cerrarEntityManagerFactory() {
        emf.close();
    }

//    @Override
//    public boolean editarOrden(Pago pago) {
//        PagoJpaController adaptado = new PagoJpaController(emf);
//        boolean exito = false;
//        try {
//            adaptado.edit(pago);
//            exito = true;
//        } catch (Exception ex) {
//            JOptionPane.showMessageDialog(null, "Error tipo: " + ex,
//                    "Error", JOptionPane.ERROR_MESSAGE);
//        }
//        return exito;
//    }
    @Override
    public List listadoFacturas() {
        FacturaJpaController adaptado = new FacturaJpaController(emf);
        return adaptado.findFacturaEntities();
//         EntityManager manejador = null;
//        try {
//            manejador = emf.createEntityManager();
//            Query query = manejador.createQuery("FROM facturas");
//
//            return query.getResultList();
//        } finally {
//
//            if (manejador == null) {
//                manejador.close();
//
//            }
//
//        }

    }

    public List listarPagosFactura(int num_factura) {
        EntityManager manejador = null;
        try {
            manejador = emf.createEntityManager();
            Query query = manejador.createQuery("FROM Pago WHERE factura_nofactura =:num_factura");
            query.setParameter("num_factura", num_factura);
            return query.getResultList();
        } finally {

            if (manejador == null) {
                manejador.close();

            }

        }
    }

    public ArrayList<Object> consultaReporteIngresosMensual(String mes, String anio) {

//        String consulta = "select sum(total_pago), fecha_pago from facturas where extract(month from fecha_pago::date) = '"+mes+
//                "' and extract(year from fecha_pago::date) = '"+anio+"' group by fecha_pago;";
//        String consulta2 = "select total_pago from facturas";
//        System.out.println(consulta2);
//        Query query = manejador.createQuery("select sum(total_pago), fecha_pago from facturas where extract(month from fecha_pago::date) =: mes and extract(year from fecha_pago::date) =: anio group by fecha_pago;");
//        query.setParameter("mes", mes);
//        query.setParameter("anio", anio);
        EntityManager manejador = null;
        manejador = emf.createEntityManager();
        manejador.getTransaction().begin();
        String consulta = "select sum(total_pago) as ingresos, fecha_pago as fecha from facturas where extract(month from cast(fecha_pago as date)) = " + mes
                + " and extract(year from cast(fecha_pago as date)) = " + anio + " group by fecha_pago;";
        Query query = manejador.createNativeQuery(consulta);

        System.out.println(consulta);

        ArrayList lista = new ArrayList(query.getResultList());

        System.out.println("tam " + lista.size());

        String info = "";
        for (Object object : lista) {
            Object[] objetos = (Object[]) object;
            info += "fecha: " + objetos[1] + "\t sum: " + objetos[0] + "\n";
        }
        System.out.println("hice la consulta");
        System.out.println(info);
        manejador.getTransaction().commit();
        emf.close();

        return lista;
    }

    @Override
    public boolean editarFactura(Factura factura) {
        boolean exito = false;
        EntityManager manejador = null;

        try {
            manejador = emf.createEntityManager();
            manejador.getTransaction().begin();
            manejador.merge(factura);
            manejador.getTransaction().commit();
            exito = true;
        } finally {
            if (manejador != null) {
                manejador.close();
            }
        }
        return exito;
    }

}
