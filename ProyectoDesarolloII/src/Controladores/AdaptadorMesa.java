/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Controladores.Abstractas.AccesoBdMesa;
import Controladores.exceptions.NonexistentEntityException;
import Modelos.Mesa;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.JOptionPane;

/**
 *
 * @author jhonfmr
 */
public class AdaptadorMesa extends AccesoBdMesa {

    private EntityManagerFactory emf;

    public AdaptadorMesa() {
        this.emf = Persistence.createEntityManagerFactory("Persistencia");
    }

    @Override
    public List listadoMesas() {
        MesaJpaController adaptado = new MesaJpaController(emf);
        return adaptado.findMesaEntities();
    }

    @Override
    public void cerrarEntityManagerFactory() {
        emf.close();
    }

    @Override
    public void almacenarMesa(Mesa mesa) {
        MesaJpaController adaptado = new MesaJpaController(emf);
        adaptado.create(mesa);
    }

    @Override
    public boolean editarMesa(Mesa mesa) {
        MesaJpaController adaptado = new MesaJpaController(emf);
        
        boolean exito = false;
        try {
            adaptado.edit(mesa);
            exito = true;
        } catch (NonexistentEntityException ex) {
            JOptionPane.showMessageDialog(null, "Error con la entidad de tipo: " + ex,
                    "Error", JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error tipo: " + ex,
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return exito;
    }

    @Override
    public Mesa buscarMesa(long id) {
        MesaJpaController adaptado = new MesaJpaController(emf);
        return adaptado.findMesa(id);
    }

    @Override
    public List buscarMesa(String numero) {
        EntityManager manejador = null;
        try {
            manejador = emf.createEntityManager();
            manejador.getTransaction().begin();
            Query query = manejador.createQuery("FROM Mesa WHERE numero_mesa = :numero");
            query.setParameter("numero", numero);
            manejador.getTransaction().commit();
            return query.getResultList();
        } finally {
            if (manejador == null) {
                manejador.close();
            }
        }
    }

}
