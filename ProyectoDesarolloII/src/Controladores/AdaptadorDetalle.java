
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Controladores.Abstractas.AccesoBdDetalle;
import Controladores.exceptions.NonexistentEntityException;
import Modelos.Detalle;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JOptionPane;

/**
 *
 * @author invitado
 */
public class AdaptadorDetalle extends AccesoBdDetalle {

    private EntityManagerFactory emf;

    public AdaptadorDetalle() {
        this.emf = Persistence.createEntityManagerFactory("Persistencia");
    }

    @Override
    public void cerrarEntityManagerFactory() {
        emf.close();
    }

    @Override
    public void almacenarDetalleOrden(Detalle detalleOrden) {
        DetalleJpaController adaptado = new DetalleJpaController(emf);
        adaptado.create(detalleOrden);
    }

    @Override
    public void EliminarDetalle(Detalle detalleOrden) {
        EntityManager manejador = null;
        try {
            manejador = emf.createEntityManager();
            manejador.getTransaction().begin();
            Detalle detalle = manejador.getReference(Detalle.class, detalleOrden.getId());
            manejador.remove(detalle);
            manejador.getTransaction().commit();
        } finally {
            if (manejador == null) {
                manejador.close();
            }
        }
    }

    @Override
    public Detalle buscarDetalleOrden(long id) {
        DetalleJpaController adaptado = new DetalleJpaController(emf);
        return adaptado.findDetalle(id);
    }
    
    
    @Override
    public boolean editarDetalle(Detalle detalleOrden){
        DetalleJpaController adaptado = new DetalleJpaController(emf);
        boolean exito = false;
        try {
            adaptado.edit(detalleOrden);
            exito = true;
        } catch (NonexistentEntityException ex) {
            JOptionPane.showMessageDialog(null, "Error con la entidad de tipo: " + ex,
                    "Error", JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error tipo: " + ex,
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        return exito;
    }

}
