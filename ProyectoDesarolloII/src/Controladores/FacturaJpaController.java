/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Controladores.exceptions.IllegalOrphanException;
import Controladores.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Modelos.Empleado;
import Modelos.Pago;
import java.util.ArrayList;
import java.util.Collection;
import Modelos.DetalleFactura;
import Modelos.Factura;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author john
 */
public class FacturaJpaController implements Serializable {

    public FacturaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Factura factura) {
        if (factura.getPagoColeccion() == null) {
            factura.setPagoColeccion(new ArrayList<Pago>());
        }
        if (factura.getDetalleFacturaColeccion() == null) {
            factura.setDetalleFacturaColeccion(new ArrayList<DetalleFactura>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Empleado cajero = factura.getCajero();
            if (cajero != null) {
                cajero = em.getReference(cajero.getClass(), cajero.getId());
                factura.setCajero(cajero);
            }
            Collection<Pago> attachedPagoColeccion = new ArrayList<Pago>();
            for (Pago pagoColeccionPagoToAttach : factura.getPagoColeccion()) {
                pagoColeccionPagoToAttach = em.getReference(pagoColeccionPagoToAttach.getClass(), pagoColeccionPagoToAttach.getId());
                attachedPagoColeccion.add(pagoColeccionPagoToAttach);
            }
            factura.setPagoColeccion(attachedPagoColeccion);
            Collection<DetalleFactura> attachedDetalleFacturaColeccion = new ArrayList<DetalleFactura>();
            for (DetalleFactura detalleFacturaColeccionDetalleFacturaToAttach : factura.getDetalleFacturaColeccion()) {
                detalleFacturaColeccionDetalleFacturaToAttach = em.getReference(detalleFacturaColeccionDetalleFacturaToAttach.getClass(), detalleFacturaColeccionDetalleFacturaToAttach.getId());
                attachedDetalleFacturaColeccion.add(detalleFacturaColeccionDetalleFacturaToAttach);
            }
            factura.setDetalleFacturaColeccion(attachedDetalleFacturaColeccion);
            em.persist(factura);
            if (cajero != null) {
                cajero.getFacturaColeccion().add(factura);
                cajero = em.merge(cajero);
            }
            for (Pago pagoColeccionPago : factura.getPagoColeccion()) {
                Factura oldFacturaOfPagoColeccionPago = pagoColeccionPago.getFactura();
                pagoColeccionPago.setFactura(factura);
                pagoColeccionPago = em.merge(pagoColeccionPago);
                if (oldFacturaOfPagoColeccionPago != null) {
                    oldFacturaOfPagoColeccionPago.getPagoColeccion().remove(pagoColeccionPago);
                    oldFacturaOfPagoColeccionPago = em.merge(oldFacturaOfPagoColeccionPago);
                }
            }
            for (DetalleFactura detalleFacturaColeccionDetalleFactura : factura.getDetalleFacturaColeccion()) {
                Factura oldFacturaOfDetalleFacturaColeccionDetalleFactura = detalleFacturaColeccionDetalleFactura.getFactura();
                detalleFacturaColeccionDetalleFactura.setFactura(factura);
                detalleFacturaColeccionDetalleFactura = em.merge(detalleFacturaColeccionDetalleFactura);
                if (oldFacturaOfDetalleFacturaColeccionDetalleFactura != null) {
                    oldFacturaOfDetalleFacturaColeccionDetalleFactura.getDetalleFacturaColeccion().remove(detalleFacturaColeccionDetalleFactura);
                    oldFacturaOfDetalleFacturaColeccionDetalleFactura = em.merge(oldFacturaOfDetalleFacturaColeccionDetalleFactura);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Factura factura) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Factura persistentFactura = em.find(Factura.class, factura.getNoFactura());
            Empleado cajeroOld = persistentFactura.getCajero();
            Empleado cajeroNew = factura.getCajero();
            String formaPagoNew = factura.getFormaPago();
            String formaPagoOld = persistentFactura.getFormaPago();
            Collection<Pago> pagoColeccionOld = persistentFactura.getPagoColeccion();
            Collection<Pago> pagoColeccionNew = factura.getPagoColeccion();
            Collection<DetalleFactura> detalleFacturaColeccionOld = persistentFactura.getDetalleFacturaColeccion();
            Collection<DetalleFactura> detalleFacturaColeccionNew = factura.getDetalleFacturaColeccion();
            List<String> illegalOrphanMessages = null;
            for (Pago pagoColeccionOldPago : pagoColeccionOld) {
                if (!pagoColeccionNew.contains(pagoColeccionOldPago)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Pago " + pagoColeccionOldPago + " since its factura field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (cajeroNew != null) {
                cajeroNew = em.getReference(cajeroNew.getClass(), cajeroNew.getId());
                factura.setCajero(cajeroNew);
            }
            
            if(formaPagoNew != null){
                factura.setFormaPago(formaPagoNew);
                factura = em.merge(factura);
            }
            
            Collection<Pago> attachedPagoColeccionNew = new ArrayList<Pago>();
            for (Pago pagoColeccionNewPagoToAttach : pagoColeccionNew) {
                pagoColeccionNewPagoToAttach = em.getReference(pagoColeccionNewPagoToAttach.getClass(), pagoColeccionNewPagoToAttach.getId());
                attachedPagoColeccionNew.add(pagoColeccionNewPagoToAttach);
            }
            pagoColeccionNew = attachedPagoColeccionNew;
            factura.setPagoColeccion(pagoColeccionNew);
            Collection<DetalleFactura> attachedDetalleFacturaColeccionNew = new ArrayList<DetalleFactura>();
            for (DetalleFactura detalleFacturaColeccionNewDetalleFacturaToAttach : detalleFacturaColeccionNew) {
                detalleFacturaColeccionNewDetalleFacturaToAttach = em.getReference(detalleFacturaColeccionNewDetalleFacturaToAttach.getClass(), detalleFacturaColeccionNewDetalleFacturaToAttach.getId());
                attachedDetalleFacturaColeccionNew.add(detalleFacturaColeccionNewDetalleFacturaToAttach);
            }
            detalleFacturaColeccionNew = attachedDetalleFacturaColeccionNew;
            factura.setDetalleFacturaColeccion(detalleFacturaColeccionNew);
            factura = em.merge(factura);
            if (cajeroOld != null && !cajeroOld.equals(cajeroNew)) {
                cajeroOld.getFacturaColeccion().remove(factura);
                cajeroOld = em.merge(cajeroOld);
            }
            if (cajeroNew != null && !cajeroNew.equals(cajeroOld)) {
                cajeroNew.getFacturaColeccion().add(factura);
                cajeroNew = em.merge(cajeroNew);
            }
            
            
            for (Pago pagoColeccionNewPago : pagoColeccionNew) {
                if (!pagoColeccionOld.contains(pagoColeccionNewPago)) {
                    Factura oldFacturaOfPagoColeccionNewPago = pagoColeccionNewPago.getFactura();
                    pagoColeccionNewPago.setFactura(factura);
                    pagoColeccionNewPago = em.merge(pagoColeccionNewPago);
                    if (oldFacturaOfPagoColeccionNewPago != null && !oldFacturaOfPagoColeccionNewPago.equals(factura)) {
                        oldFacturaOfPagoColeccionNewPago.getPagoColeccion().remove(pagoColeccionNewPago);
                        oldFacturaOfPagoColeccionNewPago = em.merge(oldFacturaOfPagoColeccionNewPago);
                    }
                }
            }
            for (DetalleFactura detalleFacturaColeccionOldDetalleFactura : detalleFacturaColeccionOld) {
                if (!detalleFacturaColeccionNew.contains(detalleFacturaColeccionOldDetalleFactura)) {
                    detalleFacturaColeccionOldDetalleFactura.setFactura(null);
                    detalleFacturaColeccionOldDetalleFactura = em.merge(detalleFacturaColeccionOldDetalleFactura);
                }
            }
            for (DetalleFactura detalleFacturaColeccionNewDetalleFactura : detalleFacturaColeccionNew) {
                if (!detalleFacturaColeccionOld.contains(detalleFacturaColeccionNewDetalleFactura)) {
                    Factura oldFacturaOfDetalleFacturaColeccionNewDetalleFactura = detalleFacturaColeccionNewDetalleFactura.getFactura();
                    detalleFacturaColeccionNewDetalleFactura.setFactura(factura);
                    detalleFacturaColeccionNewDetalleFactura = em.merge(detalleFacturaColeccionNewDetalleFactura);
                    if (oldFacturaOfDetalleFacturaColeccionNewDetalleFactura != null && !oldFacturaOfDetalleFacturaColeccionNewDetalleFactura.equals(factura)) {
                        oldFacturaOfDetalleFacturaColeccionNewDetalleFactura.getDetalleFacturaColeccion().remove(detalleFacturaColeccionNewDetalleFactura);
                        oldFacturaOfDetalleFacturaColeccionNewDetalleFactura = em.merge(oldFacturaOfDetalleFacturaColeccionNewDetalleFactura);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = factura.getNoFactura();
                if (findFactura(id) == null) {
                    throw new NonexistentEntityException("The factura with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Factura factura;
            try {
                factura = em.getReference(Factura.class, id);
                factura.getNoFactura();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The factura with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Pago> pagoColeccionOrphanCheck = factura.getPagoColeccion();
            for (Pago pagoColeccionOrphanCheckPago : pagoColeccionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Factura (" + factura + ") cannot be destroyed since the Pago " + pagoColeccionOrphanCheckPago + " in its pagoColeccion field has a non-nullable factura field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Empleado cajero = factura.getCajero();
            if (cajero != null) {
                cajero.getFacturaColeccion().remove(factura);
                cajero = em.merge(cajero);
            }
            Collection<DetalleFactura> detalleFacturaColeccion = factura.getDetalleFacturaColeccion();
            for (DetalleFactura detalleFacturaColeccionDetalleFactura : detalleFacturaColeccion) {
                detalleFacturaColeccionDetalleFactura.setFactura(null);
                detalleFacturaColeccionDetalleFactura = em.merge(detalleFacturaColeccionDetalleFactura);
            }
            em.remove(factura);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Factura> findFacturaEntities() {
        return findFacturaEntities(true, -1, -1);
    }

    public List<Factura> findFacturaEntities(int maxResults, int firstResult) {
        return findFacturaEntities(false, maxResults, firstResult);
    }

    private List<Factura> findFacturaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Factura.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Factura findFactura(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Factura.class, id);
        } finally {
            em.close();
        }
    }

    public int getFacturaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Factura> rt = cq.from(Factura.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
