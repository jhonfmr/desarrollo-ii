/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cajero;

import java.util.Observable;

/**
 *
 * @author invitado
 */
public class ObservadoCajero extends Observable {
    
    private boolean pagoAñadido;

    public ObservadoCajero() {
        pagoAñadido = false;
    }

    public boolean isPagoAñadido() {
        return pagoAñadido;
    }

    public void setPagoAñadido(boolean pago) {
        this.pagoAñadido = pago;
        setChanged();
        notifyObservers();
    }

}
