/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cajero;

import Controladores.AdaptadorPago;
import Modelos.Empleado;
import Modelos.Orden;
import Modelos.Factura;
import Modelos.Pago;
import java.time.LocalDate;

/**
 *
 * @author annie
 */
public class GenerarPagoEfectivo  extends GenerarPago {
    
    private double dineroRecibido; 
    private Factura factura;
     
    /**
     * Responsable: Annie Muñoz
     * Método constructor que inicializa los parámetros 
     */
    public  GenerarPagoEfectivo(Object[] parametros){
        dineroRecibido =Double.parseDouble(parametros[0].toString());;
        factura = (Factura)parametros[1];
    }
    
   
    /**
     * Responsable: Annie Muñoz
     * Método que agrega un pago a la factura y almacena este pago en la base de datos
     */
    @Override
    public void RealizarPago() {         
        
        System.out.println("PAGO EFECTIVO");
        Pago pago = new Pago(dineroRecibido, Pago.FormaPago.EFECTIVO);
        pago.setFactura(factura);
        AdaptadorPago adaptador =  new AdaptadorPago();
        adaptador.almacenarPago(pago); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
