/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cajero;

import Controladores.AdaptadorPago;
import Modelos.Empleado;
import Modelos.Orden;
import Modelos.Factura;
import Modelos.Pago;
import Modelos.Factura;
import java.time.LocalDate;

/**
 *
 * @author annie
 */
public class GenerarPagoTarjeta extends GenerarPago {

    private double monto;
    Factura factura;

    /**
     * Responsable: Annie Muñoz
     * Método constructor que inicializa los parámetros 
     */
    public GenerarPagoTarjeta(Object[] parametros) {

        monto = Double.parseDouble(parametros[0].toString());
        factura = (Factura) parametros[1];
    }
    
    /**
     * Responsable: Annie Muñoz
     * Método que agrega un pago a la factura y almacena este pago en la base de datos
     */
    @Override
    public void RealizarPago() {
        System.out.println("PAGO CON TARJETA");
        Pago pago = new Pago(monto, Pago.FormaPago.TARJETA);
        pago.setFactura(factura);
        AdaptadorPago adaptador = new AdaptadorPago();
        adaptador.almacenarPago(pago);
    }

}
