/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cajero;

/**
 *
 * @author annie
 */
public class FabricaPagos {
    
    /**
     * Responsable: Annie Muñoz
     * @param tipo_pago
     * @param parametros
     * @return GenerarPago
     * Metodo que retorna un objeto de una clase heredera de GenerarPago
     * dependiendo del tipo de pago ingresado como parámetro
     */
     public GenerarPago getPago(int tipo_pago, Object[] parametros){
		
      if(tipo_pago==1){//Pago con tarjeta
    	  
    	  return (new GenerarPagoTarjeta(parametros));
      } if (tipo_pago == 2){//Pago en efectivo
      
          return (new GenerarPagoEfectivo(parametros));
            
      }
      return (new GenerarPagoTarjeta(parametros));
    }
}
