/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cajero;

/**
 *
 * @author annie
 */
abstract class GenerarPago {
   
    
    public GenerarPago(){    
    
    }
    /**
     * Responsable: Annie Muñoz
     * Método abstracto que realizara el pago en la base de datos
     */
    public  abstract void RealizarPago();    
    
    
}
