package Modelos;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Item.class)
public abstract class Item_ {

	public static volatile SingularAttribute<Item, String> descripcion;
	public static volatile SingularAttribute<Item, Double> precio;
	public static volatile SingularAttribute<Item, String> estado;
	public static volatile CollectionAttribute<Item, Detalle> detalleColeccion;
	public static volatile SingularAttribute<Item, byte[]> foto;
	public static volatile SingularAttribute<Item, String> categoria;
	public static volatile SingularAttribute<Item, Long> id;
	public static volatile SingularAttribute<Item, String> nombre;

}

