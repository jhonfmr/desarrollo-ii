package Modelos;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Pago.class)
public abstract class Pago_ {

	public static volatile SingularAttribute<Pago, String> cedulaCLiente;
	public static volatile SingularAttribute<Pago, Double> total;
	public static volatile SingularAttribute<Pago, String> estado;
	public static volatile SingularAttribute<Pago, String> hora;
	public static volatile SingularAttribute<Pago, Long> noFactura;
	public static volatile SingularAttribute<Pago, Orden> orden;
	public static volatile SingularAttribute<Pago, String> formaPago;
	public static volatile SingularAttribute<Pago, LocalDate> fechaPago;
	public static volatile SingularAttribute<Pago, Empleado> cajero;

}

