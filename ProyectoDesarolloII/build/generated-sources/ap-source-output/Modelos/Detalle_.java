package Modelos;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Detalle.class)
public abstract class Detalle_ {

	public static volatile SingularAttribute<Detalle, Item> item;
	public static volatile SingularAttribute<Detalle, Long> id;
	public static volatile SingularAttribute<Detalle, Integer> cantidad;
	public static volatile SingularAttribute<Detalle, Orden> orden;

}

