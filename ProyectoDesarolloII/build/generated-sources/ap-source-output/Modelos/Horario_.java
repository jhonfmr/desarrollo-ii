package Modelos;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Horario.class)
public abstract class Horario_ {

	public static volatile CollectionAttribute<Horario, Turno> turnoColeccion;
	public static volatile SingularAttribute<Horario, String> hora_inicio;
	public static volatile SingularAttribute<Horario, String> hora_fin;
	public static volatile SingularAttribute<Horario, Long> id;
	public static volatile SingularAttribute<Horario, String> dia;

}

