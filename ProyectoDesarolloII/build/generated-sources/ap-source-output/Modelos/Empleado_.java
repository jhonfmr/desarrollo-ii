package Modelos;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Empleado.class)
public abstract class Empleado_ {

	public static volatile CollectionAttribute<Empleado, Orden> ordenColeccion;
	public static volatile SingularAttribute<Empleado, String> estado;
	public static volatile SingularAttribute<Empleado, String> numeroIdentificacion;
	public static volatile SingularAttribute<Empleado, String> direccion;
	public static volatile CollectionAttribute<Empleado, Pago> pagoColeccion;
	public static volatile SingularAttribute<Empleado, String> nombre;
	public static volatile CollectionAttribute<Empleado, Turno> turnoColeccion;
	public static volatile SingularAttribute<Empleado, String> tipoDocumento;
	public static volatile SingularAttribute<Empleado, byte[]> foto;
	public static volatile SingularAttribute<Empleado, String> apellido;
	public static volatile SingularAttribute<Empleado, String> correo;
	public static volatile SingularAttribute<Empleado, String> celular;
	public static volatile SingularAttribute<Empleado, String> usuario;
	public static volatile SingularAttribute<Empleado, Long> id;
	public static volatile SingularAttribute<Empleado, String> cargo;
	public static volatile SingularAttribute<Empleado, String> telefono;
	public static volatile SingularAttribute<Empleado, String> contraseña;

}

