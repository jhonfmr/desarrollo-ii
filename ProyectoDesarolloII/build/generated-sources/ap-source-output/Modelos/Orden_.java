package Modelos;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Orden.class)
public abstract class Orden_ {

	public static volatile SingularAttribute<Orden, Boolean> tipo;
	public static volatile SingularAttribute<Orden, String> estado;
	public static volatile SingularAttribute<Orden, Empleado> mesero;
	public static volatile CollectionAttribute<Orden, Detalle> detalleColeccion;
	public static volatile SingularAttribute<Orden, String> fin_servicio;
	public static volatile SingularAttribute<Orden, String> mesa;
	public static volatile SingularAttribute<Orden, Long> id;
	public static volatile SingularAttribute<Orden, String> inicio_servicio;

}

